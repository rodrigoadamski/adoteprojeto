function jobsearch_loc_levels_names_to_address(rand_num) {
    var loc_loc_1 = jQuery('#location_location1_' + rand_num).val();
    var loc_loc_2 = '';
    var loc_loc_3 = '';

    if (jQuery('#location_location2_' + rand_num).length > 0) {
        loc_loc_2 = jQuery('#location_location2_' + rand_num).val();
    }
    if (jQuery('#location_location3_' + rand_num).length > 0) {
        loc_loc_3 = jQuery('#location_location3_' + rand_num).val();
    }

    var loc_request = jQuery.ajax({
        url: jobsearch_location_common_vars.ajax_url,
        method: "POST",
        data: {
            'loc_loc_1': loc_loc_1,
            'loc_loc_2': loc_loc_2,
            'loc_loc_3': loc_loc_3,
            'action': 'jobsearch_loc_levels_names_to_address'
        },
        dataType: "json"
    });
    loc_request.done(function (response) {
        if (typeof response.locadres !== 'undefined' && response.locadres != '') {
            jQuery('#jobsearch_location_address_' + rand_num).val(response.locadres);
            jQuery('#jobsearch_location_address_' + rand_num).trigger('change');
        }
    });
}

jQuery('.location_location1').on('change', function (e) {
    e.preventDefault();
    var this_id = jQuery(this).data('randid'),
            nextfieldelement = jQuery(this).data('nextfieldelement'),
            nextfieldval = jQuery(this).data('nextfieldval'),
            ajax_url = jobsearch_location_common_vars.ajax_url,
            location_location1 = jQuery('#location_location1_' + this_id),
            location_location2 = jQuery('#location_location2_' + this_id);
    jQuery('.location_location2_' + this_id).html('<i class="fa fa-refresh fa-spin"></i>');
    
    jobsearch_loc_levels_names_to_address(this_id);
    
    var request = jQuery.ajax({
        url: ajax_url,
        method: "POST",
        data: {
            location_location: location_location1.val(),
            nextfieldelement: nextfieldelement,
            nextfieldval: nextfieldval,
            action: 'jobsearch_location_load_location2_data',
        },
        dataType: "json"
    });

    request.done(function (response) {
        if ('undefined' !== typeof response.html) {
            if (jQuery('.location_location2_selectize').length > 0) {
                if (jQuery('.location_location2_selectize').hasClass('location2_selectize_ajax')) {
                    jQuery('.location2_selectize_ajax').selectize()[0].selectize.destroy();
                }
            }
            location_location2.html(response.html);
            if (typeof location_location2.parent('.jobsearch-profile-select').find('.selectize-control') !== 'undefined') {
                location_location2.parent('.jobsearch-profile-select').find('.selectize-control').remove();
                location_location2.removeAttr('style');
                location_location2.removeAttr('tabindex');
                location_location2.removeClass('location2_selectize_ajax');
                location_location2.removeClass('selectized');
            }
            jQuery('.location_location2_' + this_id).html('');
            if (nextfieldval != '') {
                jQuery('.location_location2').trigger('change');
            }
            //
            if (jQuery('.location_location2_selectize').length > 0) {
                if (!jQuery('.location_location2_selectize').hasClass('location2_selectize_ajax')) {
                    jQuery('.location_location2_selectize').addClass('location2_selectize_ajax');

                    jQuery('.location2_selectize_ajax').selectize({
                        //allowEmptyOption: true,
                    });
                }
            }
        }
    });

    request.fail(function (jqXHR, textStatus) {
    });
    return false;

});

jQuery('.location_location2').on('change', function (e) {
    e.preventDefault();
    var this_id = jQuery(this).data('randid'),
            nextfieldelement = jQuery(this).data('nextfieldelement'),
            nextfieldval = jQuery(this).data('nextfieldval'),
            ajax_url = jobsearch_location_common_vars.ajax_url,
            location_location2 = jQuery('#location_location2_' + this_id),
            location_location3 = jQuery('#location_location3_' + this_id);
    jQuery('.location_location3_' + this_id).html('<i class="fa fa-refresh fa-spin"></i>');
    
    jobsearch_loc_levels_names_to_address(this_id);
    
    var request = jQuery.ajax({
        url: ajax_url,
        method: "POST",
        data: {
            location_location: location_location2.val(),
            nextfieldelement: nextfieldelement,
            nextfieldval: nextfieldval,
            action: 'jobsearch_location_load_location2_data',
        },
        dataType: "json"
    });

    request.done(function (response) {
        if ('undefined' !== typeof response.html) {
            if (jQuery('.location_location3_selectize').length > 0) {
                if (jQuery('.location_location3_selectize').hasClass('location3_selectize_ajax')) {
                    jQuery('.location3_selectize_ajax').selectize()[0].selectize.destroy();
                }
            }
            location_location3.html(response.html);
            if (typeof location_location3.parent('.jobsearch-profile-select').find('.selectize-control') !== 'undefined') {
                location_location3.parent('.jobsearch-profile-select').find('.selectize-control').remove();
                location_location3.removeAttr('style');
                location_location3.removeAttr('tabindex');
                location_location3.removeClass('location2_selectize_ajax');
                location_location3.removeClass('selectized');
            }
            jQuery('.location_location3_' + this_id).html('');
            if (nextfieldval != '') {
                jQuery('.location_location3').trigger('change');
            }
            //
            if (jQuery('.location_location3_selectize').length > 0) {
                if (!jQuery('.location_location3_selectize').hasClass('location3_selectize_ajax')) {
                    jQuery('.location_location3_selectize').addClass('location3_selectize_ajax');

                    jQuery('.location3_selectize_ajax').selectize({
                        //allowEmptyOption: true,
                    });
                }
            }
        }
    });

    request.fail(function (jqXHR, textStatus) {
    });
    return false;

});

jQuery('.location_location3').on('change', function (e) {
    e.preventDefault();
    var this_id = jQuery(this).data('randid'),
            nextfieldelement = jQuery(this).data('nextfieldelement'),
            nextfieldval = jQuery(this).data('nextfieldval'),
            ajax_url = jobsearch_location_common_vars.ajax_url,
            location_location3 = jQuery('#location_location3_' + this_id),
            location_location4 = jQuery('#location_location4_' + this_id);
    jQuery('.location_location4_' + this_id).html('<i class="fa fa-refresh fa-spin"></i>');
    
    jobsearch_loc_levels_names_to_address(this_id);
    
    var request = jQuery.ajax({
        url: ajax_url,
        method: "POST",
        data: {
            location_location: location_location3.val(),
            nextfieldelement: nextfieldelement,
            nextfieldval: nextfieldval,
            action: 'jobsearch_location_load_location2_data',
        },
        dataType: "json"
    });

    request.done(function (response) {
        if ('undefined' !== typeof response.html) {
            if (jQuery('.location_location4_selectize').length > 0) {
                if (jQuery('.location_location4_selectize').hasClass('location4_selectize_ajax')) {
                    jQuery('.location4_selectize_ajax').selectize()[0].selectize.destroy();
                }
            }
            location_location4.html(response.html);
            if (typeof location_location4.parent('.jobsearch-profile-select').find('.selectize-control') !== 'undefined') {
                location_location4.parent('.jobsearch-profile-select').find('.selectize-control').remove();
                location_location4.removeAttr('style');
                location_location4.removeAttr('tabindex');
                location_location4.removeClass('location2_selectize_ajax');
                location_location4.removeClass('selectized');
            }
            jQuery('.location_location4_' + this_id).html('');
            //
            if (jQuery('.location_location4_selectize').length > 0) {
                if (!jQuery('.location_location4_selectize').hasClass('location4_selectize_ajax')) {
                    jQuery('.location_location4_selectize').addClass('location4_selectize_ajax');

                    jQuery('.location4_selectize_ajax').selectize({
                        //allowEmptyOption: true,
                    });
                }
            }
        }
    });

    request.fail(function (jqXHR, textStatus) {
    });
    return false;

});