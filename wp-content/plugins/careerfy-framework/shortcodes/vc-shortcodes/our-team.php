<?php

/**
 * Our Team Shortcode
 * @return html
 */
add_shortcode('careerfy_our_team', 'careerfy_our_team_shortcode');

function careerfy_our_team_shortcode($atts, $content = '') {

    wp_enqueue_script('careerfy-slick-slider');

    $html = '
    <div class="careerfy-service-slider">
        ' . do_shortcode($content) . '
    </div>' . "\n";

    return $html;
}

add_shortcode('careerfy_our_team_item', 'careerfy_our_team_item_shortcode');

function careerfy_our_team_item_shortcode($atts) {

    extract(shortcode_atts(array(
        'team_img' => '',
        'team_title' => '',
        'team_pos' => '',
                    ), $atts));

    $html = '
    <div class="careerfy-service-slider-layer">
        <a><img src="' . $team_img . '" alt=""></a>
        <span>' . $team_title . ' <small>' . $team_pos . '</small></span>
    </div>';

    return $html;
}
