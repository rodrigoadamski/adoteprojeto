<?php
/**
 * FAQs Shortcode
 * @return html
 */
add_shortcode('careerfy_faqs', 'careerfy_faqs_shortcode');

function careerfy_faqs_shortcode($atts, $content = '') {

    extract(shortcode_atts(array(
        'ques_title' => '',
        'op_first_q' => 'yes',
        'faq_cat' => '',
        'faq_excerpt' => '20',
        'faq_order' => 'DESC',
        'faq_orderby' => 'date',
        'num_of_faqs' => '10',
                    ), $atts));

    $faq_shortcode_counter = 1;
    $faq_shortcode_rand_id = rand(10000000, 99999999);

    ob_start();

    if ($ques_title != '') {
        ?>
        <div class="careerfy-section-title"><h2><?php echo ($ques_title) ?></h2></div>
        <?php
    }

    $num_of_faqs = $num_of_faqs == '' ? -1 : absint($num_of_faqs);
    $args = array(
        'post_type' => 'faq',
        'posts_per_page' => $num_of_faqs,
        'post_status' => 'publish',
        'order' => $faq_order,
        'orderby' => $faq_orderby,
    );

    if ($faq_cat && $faq_cat != '' && $faq_cat != '0') {
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'faq-category',
                'field' => 'slug',
                'terms' => $faq_cat,
            ),
        );
    }

    $faq_query = new WP_Query($args);
    $total_posts = $faq_query->found_posts;

    if ($faq_query->have_posts()) {
        ?>
        <div class="panel-group careerfy-accordion" id="accordion-<?php echo ($faq_shortcode_rand_id) ?>">
            <?php
            while ($faq_query->have_posts()) : $faq_query->the_post();

                $item_rand_id = rand(10000000, 99999999);

                $open_faq_item = false;
                if ($op_first_q == 'yes' && $faq_shortcode_counter == 1) {
                    $open_faq_item = true;
                }
                ?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a <?php echo ($open_faq_item ? '' : 'class="collapsed"') ?> role="button" data-toggle="collapse" data-parent="#accordion-<?php echo ($faq_shortcode_rand_id) ?>" href="#collapse-<?php echo ($item_rand_id) ?>" aria-expanded="true" aria-controls="collapse-<?php echo ($item_rand_id) ?>">
                                <i class="careerfy-icon careerfy-arrows"></i> Q. <?php echo get_the_title(get_the_ID()) ?>
                            </a>
                        </h4>
                    </div>
                    <div id="collapse-<?php echo ($item_rand_id) ?>" class="panel-collapse collapse <?php echo ($open_faq_item ? 'in' : '') ?>">
                        <div class="panel-body">
                            <?php echo careerfy_excerpt($faq_excerpt) ?>
                        </div>
                    </div>
                </div>
                <?php
                $faq_shortcode_counter++;

            endwhile;
            wp_reset_postdata();
            ?>
        </div>
        <?php
    }

    $html = ob_get_clean();
    return $html;
}
