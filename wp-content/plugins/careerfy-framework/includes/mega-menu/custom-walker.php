<?php

/**
 * Custom Walker
 * @access      public
 * @since       1.0 
 * @return      void
 */
class careerfy_mega_menu_walker extends Walker_Nav_Menu {

    private $CurrentItem, $CategoryMenu, $menu_style;
    public $parent_menu_item_id = 0;
    public $child_items_count = 0;
    public $child_menu_item_id = 0;
    public $view = '';

    function __Construct($view = '') {
        $this->view = $view;
    }

    // Start function for Mega menu
    function careerfy_menu_start() {
        $sub_class = $last = '';
        $count_menu_posts = 0;
        $mega_menu_output = '';
    }

    // Start function For Mega menu level
    function start_lvl(&$output, $depth = 0, $args = array(), $id = 0) {
        $indent = str_repeat("\t", $depth);

        $output .= $this->careerfy_menu_start();
        $columns_class = $this->CurrentItem->columns;

        $careerfy_parent_id = $this->CurrentItem->menu_item_parent;

        $parent_nav_mega = get_post_meta($careerfy_parent_id, '_menu_item_megamenu', true);

        $parent_nav_mega_view = get_post_meta($careerfy_parent_id, '_menu_item_view', true);

        if ($this->CurrentItem->megamenu == 'on' && $depth == 0) {
            $output .= "\n$indent<ul class=\"careerfy-megamenu row\">\n";
        } else if ($parent_nav_mega == 'on' && $depth == 1) {
            $output .= "\n$indent<ul class=\"careerfy-megalist\">\n";
        } else {
            $output .= "\n$indent<ul class=\"sub-menu\">\n";
        }
    }

    // Start function For Mega menu level end 

    function end_lvl(&$output, $depth = 0, $args = array()) {

        $careerfy_parent_id = $this->CurrentItem->menu_item_parent;
        $parent_nav_mega = get_post_meta($this->parent_menu_item_id, '_menu_item_megamenu', true);
        $parent_nav_mega_view = get_post_meta($this->parent_menu_item_id, '_menu_item_view', true);

        $indent = str_repeat("\t", $depth);

        if ($parent_nav_mega == 'on' && $depth == 0) {
            if ($parent_nav_mega_view == 'image-text') {
                $_menu_item_image_title = get_post_meta($this->parent_menu_item_id, '_menu_item_image_title', true);
                $_menu_item_image_paragragh = get_post_meta($this->parent_menu_item_id, '_menu_item_image_paragragh', true);
                $_menu_item_image_title_2 = get_post_meta($this->parent_menu_item_id, '_menu_item_image_title_2', true);
                $_menu_item_image_img = get_post_meta($this->parent_menu_item_id, '_menu_item_image_img', true);

                if ($_menu_item_image_paragragh != '') {
                    $output .= '
					<li class="col-md-5">
						<h4>' . $_menu_item_image_title . '</h4>
						<div class="careerfy-mega-text">
							<p>' . $_menu_item_image_paragragh . '</p>
						</div>
					</li>';
                }
                if ($_menu_item_image_img != '') {
                    $output .= '
					<li class="col-md-5">
						<h4>' . $_menu_item_image_title_2 . '</h4>
						<a class="careerfy-thumbnail">
							<img src="' . $_menu_item_image_img . '" alt="">
						</a>
					</li>';
                }
            }
            if ($parent_nav_mega_view == 'video') {
                $_menu_item_video = get_post_meta($this->parent_menu_item_id, '_menu_item_video', true);
                if ($_menu_item_video != '') {
                    $output .= '
					<li class="col-md-6">
						<a class="careerfy-thumbnail">
							' . wp_oembed_get($_menu_item_video, array('height' => 300)) . '
						</a>
					</li>';
                }
            }
        }
        $output .= $indent . "</ul>\n";
        $item_id = $this->parent_menu_item_id . $this->child_menu_item_id;
    }

    // Start function For Mega menu items

    function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0) {

        global $wp_query;
        $this->CurrentItem = $item;

        $parent_nav_mega = 'off';
        $parent_item_mega_view = '';

        // show more/less more variables 
        $li_style = '';
        if ($depth == 2) {
            $this->child_items_count ++;
        } else {
            $this->child_items_count = 0;
        }

        if ($depth == 0) {
            $this->parent_menu_item_id = $item->ID;
        }
        if ($depth == 1) {
            $this->child_menu_item_id ++;
        } else if ($depth == 0) {
            $this->child_menu_item_id = 0;
        }
        //// end show more/less more

        if ($depth == 1) {
            $parent_menu_id = $item->menu_item_parent;
            $parent_nav_mega = get_post_meta($parent_menu_id, '_menu_item_megamenu', true);
            $parent_item_mega_view = get_post_meta($parent_menu_id, '_menu_item_view', true);
        }

        if (empty($args)) {
            $args = new stdClass();
        }

        $indent = ( $depth ) ? str_repeat("\t", $depth) : '';
        if ($depth == 0) {
            $class_names = $value = '';
            $mega_menu = '';
        } else if ($args->has_children) {
            $class_names = $value = '';
            $mega_menu = '';
        } else {
            $class_names = $value = $mega_menu = '';
        }
        $classes = empty($item->classes) ? array() : (array) $item->classes;

        $class_names = join(" $mega_menu ", apply_filters('nav_menu_css_class', array_filter($classes), $item));
        if ($this->CurrentItem->megamenu == 'on' && $args->has_children && $depth == 0) {
            $class_names = ' class="' . esc_attr($class_names) . ' careerfy-megamenu-li"';
        } else if ($parent_nav_mega == 'on') {
            if ($depth == 1) {
                $class_names = ' class="col-md-2"';
            } else {
                $class_names = ' class="col-md-2"';
            }
        } else {
            $class_names = ' class="' . esc_attr($class_names) . '"';
        }
        
        $output .= $indent . '<li id="menu-item-' . $item->ID . '"' . $value . $class_names . '>';
        $attributes = isset($item->attr_title) && $item->attr_title != '' ? ' title="' . esc_attr($item->attr_title) . '"' : '';
        $attributes .= isset($item->target) && $item->target != '' ? ' target="' . esc_attr($item->target) . '"' : '';
        $attributes .= isset($item->xfn) && $item->xfn != '' ? ' rel="' . esc_attr($item->xfn) . '"' : '';
        $attributes .= isset($item->url) ? ' href="' . esc_attr($item->url) . '"' : '';
        $attributes .= isset($item->description) && !empty($item->description) ? ' data-title="' . esc_attr($item->description) . '"' : '';
        $item_output = isset($args->before) ? $args->before : '';

        if ($parent_nav_mega == 'on' && $depth == 1) {
            $item_output .= '<a class="megamenu-title" ' . $attributes . '>';
        } else {
            $item_output .= '<a' . $attributes . '>';
        }
        $careerfy_link_before = isset($args->link_before) ? $args->link_before : '';
        $item_output .= $careerfy_link_before . apply_filters('the_title', $item->title, $item->ID);
        if ($this->CurrentItem->subtitle != '') {
            $item_output .= '<span>' . $this->CurrentItem->subtitle . '</span>';
        }
        $careerfy_link_after = isset($args->link_before) ? $args->link_before : '';
        $item_output .= $careerfy_link_after;
        if ($parent_nav_mega == 'on' && $depth == 1) {
             $item_output .= '</a>';
        } else {
            $item_output .= '</a>';
        }

        $item_output .= isset($args->after) ? $args->after : '';
        if (!empty($mega_menu) && empty($args->has_children) && $this->CurrentItem->megamenu == 'on') {
            $item_output .= $this->careerfy_menu_start();
        }

        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args, $id);
        
        return $output;
    }

    // Start function For Mega menu display elements

    function display_element($element, &$children_elements, $max_depth, $depth = 0, $args, &$output) {
        $id_field = $this->db_fields['id'];
        if (is_object($args[0])) {
            $args[0]->has_children = !empty($children_elements[$element->$id_field]);
        }
        return parent::display_element($element, $children_elements, $max_depth, $depth, $args, $output);
    }

}
