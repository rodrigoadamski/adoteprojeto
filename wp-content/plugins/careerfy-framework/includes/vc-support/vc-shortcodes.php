<?php

/**
 * visual composer shortcodes mapping
 * @config
 */
/**
 * list all hooks adding
 * @return hooks
 */
add_action('vc_before_init', 'careerfy_vc_blog_shortcode');
add_action('vc_before_init', 'careerfy_vc_section_heading');
add_action('vc_before_init', 'careerfy_vc_left_title_shortcode');
add_action('vc_before_init', 'careerfy_vc_button_shortcode');
add_action('vc_before_init', 'careerfy_vc_advance_search');
add_action('vc_before_init', 'careerfy_vc_job_categories');
add_action('vc_before_init', 'careerfy_vc_jobs_listing');
add_action('vc_before_init', 'careerfy_vc_employer_listing');
add_action('vc_before_init', 'careerfy_vc_candidate_listing');
add_action('vc_before_init', 'careerfy_vc_call_to_action');
add_action('vc_before_init', 'careerfy_vc_about_company');
add_action('vc_before_init', 'careerfy_vc_block_text_box');
add_action('vc_before_init', 'careerfy_vc_simple_block_text');
add_action('vc_before_init', 'careerfy_vc_find_question');
add_action('vc_before_init', 'careerfy_vc_google_map_shortcode');
add_action('vc_before_init', 'careerfy_vc_contact_information');
add_action('vc_before_init', 'careerfy_vc_about_information');
add_action('vc_before_init', 'careerfy_vc_image_banner');
add_action('vc_before_init', 'careerfy_vc_faqs_shortcode');

add_action('vc_before_init', 'careerfy_vc_counters_shortcode');
add_action('vc_before_init', 'careerfy_vc_services_shortcode');
add_action('vc_before_init', 'careerfy_vc_image_services_shortcode');
add_action('vc_before_init', 'careerfy_vc_our_partners_shortcode');
add_action('vc_before_init', 'careerfy_vc_our_team_shortcode');
add_action('vc_before_init', 'careerfy_vc_help_links_shortcode');
add_action('vc_before_init', 'careerfy_vc_testimonials_with_image');
add_action('vc_before_init', 'careerfy_vc_recent_questions');
add_action('vc_before_init', 'careerfy_vc_cv_packages');
add_action('vc_before_init', 'careerfy_vc_job_packages');
add_action('vc_before_init', 'careerfy_vc_candidate_packages');
add_action('vc_before_init', 'careerfy_vc_all_packages');
add_action('vc_before_init', 'careerfy_vc_jobs_listings_tabs');

function careerfy_vc_jobs_listings_tabs() {
    $categories = get_terms(array(
        'taxonomy' => 'sector',
        'hide_empty' => false,
    ));
    $cate_array = array();
    if (is_array($categories) && sizeof($categories) > 0) {
        foreach ($categories as $category) {
            $cate_array[$category->name] = $category->slug;
        }
    }
    $attributes = array(
        "name" => esc_html__("Jobs Listing Tabs", "careerfy-frame"),
        "base" => "jobsearch_job_listin_tabs_shortcode",
        "class" => "",
        "category" => esc_html__("Wp JobSearch", "careerfy-frame"),
        "params" => array(
            array(
                'type' => 'checkbox',
                'heading' => esc_html__("Sector", "careerfy-frame"),
                'param_name' => 'job_cats_filter',
                'value' => $cate_array,
                'description' => esc_html__("Select Sector .", "careerfy-frame")
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("job per page", "careerfy-frame"),
                'param_name' => 'job_per_page',
                'value' => '',
                'description' => esc_html__("Set number of jobs you want show for each page.", "careerfy-frame")
            ),
        )
    );
    if (function_exists('vc_map') && class_exists('JobSearch_plugin')) {
        vc_map($attributes);
    }
}

/**
 * adding blog shortcode
 * @return markup
 */
function careerfy_vc_blog_shortcode() {

    $categories = get_categories(array(
        'orderby' => 'name',
    ));

    $cate_array = array(esc_html__("Select Category", "careerfy-frame") => '');
    if (is_array($categories) && sizeof($categories) > 0) {
        foreach ($categories as $category) {
            $cate_array[$category->cat_name] = $category->slug;
        }
    }

    $attributes = array(
        "name" => esc_html__("Blog", "careerfy-frame"),
        "base" => "careerfy_blog_shortcode",
        "class" => "",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "params" => array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Style", "careerfy-frame"),
                'param_name' => 'blog_view',
                'value' => array(
                    esc_html__("Style 1", "careerfy-frame") => 'view1',
                    esc_html__("Style 2", "careerfy-frame") => 'view2',
                    esc_html__("Style 3", "careerfy-frame") => 'view3',
                    esc_html__("Style 4", "careerfy-frame") => 'view4',
                ),
                'description' => ''
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Category", "careerfy-frame"),
                'param_name' => 'blog_cat',
                'value' => $cate_array,
                'description' => esc_html__("Select Category.", "careerfy-frame")
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Excerpt Length", "careerfy-frame"),
                'param_name' => 'blog_excerpt',
                'value' => '20',
                'description' => esc_html__("Set number of words you want show for post excerpt.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Order", "careerfy-frame"),
                'param_name' => 'blog_order',
                'value' => array(
                    esc_html__("Descending", "careerfy-frame") => 'DESC',
                    esc_html__("Ascending", "careerfy-frame") => 'ASC',
                ),
                'description' => esc_html__("Choose blog list items order.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Orderby", "careerfy-frame"),
                'param_name' => 'blog_orderby',
                'value' => array(
                    esc_html__("Date", "careerfy-frame") => 'date',
                    esc_html__("Title", "careerfy-frame") => 'title',
                ),
                'description' => esc_html__("Choose blog list items orderby.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Pagination", "careerfy-frame"),
                'param_name' => 'blog_pagination',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Choose Yes if you want to show pagination for post items.", "careerfy-frame")
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Posts per Page", "careerfy-frame"),
                'param_name' => 'blog_per_page',
                'value' => '10',
                'description' => esc_html__("Set number that how much posts you want to show per page. Leave it blank for all posts on a single page.", "careerfy-frame")
            )
        )
    );

    if (function_exists('vc_map')) {
        vc_map($attributes);
    }
}

/**
 * Adding section heading shortcode
 * @return markup
 */
function careerfy_vc_section_heading() {

    $attributes = array(
        "name" => esc_html__("Section Heading", "careerfy-frame"),
        "base" => "careerfy_section_heading",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "class" => "",
        "params" => array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Style", "careerfy-frame"),
                'param_name' => 'view',
                'value' => array(
                    esc_html__("Style 1", "careerfy-frame") => 'view1',
                    esc_html__("Style 2", "careerfy-frame") => 'view2',
                    esc_html__("Style 3", "careerfy-frame") => 'view3',
                    esc_html__("Style 4", "careerfy-frame") => 'view4',
                    esc_html__("Style 5", "careerfy-frame") => 'view5',
                ),
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Title", "careerfy-frame"),
                'param_name' => 'h_title',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Fancy Title", "careerfy-frame"),
                'param_name' => 'h_fancy_title',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Color Title", "careerfy-frame"),
                'param_name' => 'hc_title',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__("Choose Title Color", "careerfy-frame"),
                'param_name' => 'hc_title_clr',
                'value' => '',
                'description' => esc_html__("This Color will apply on 'Color Title'.", "careerfy-frame"),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => esc_html__("Icon", "careerfy-frame"),
                'param_name' => 'hc_icon',
                'value' => '',
                'description' => esc_html__("This will apply on heading style 3 only.", "careerfy-frame"),
            ),
            array(
                'type' => 'textarea',
                'heading' => esc_html__("Description", "careerfy-frame"),
                'param_name' => 'h_desc',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__("Description Color", "careerfy-frame"),
                'param_name' => 'hc_dcolor',
                'value' => '',
                'description' => esc_html__("This will apply on Description only.", "careerfy-frame"),
            ),
        )
    );

    if (function_exists('vc_map')) {
        vc_map($attributes);
    }
}

/**
 * Adding Left Title shortcode
 * @return markup
 */
function careerfy_vc_left_title_shortcode() {

    $attributes = array(
        "name" => esc_html__("Left Title", "careerfy-frame"),
        "base" => "careerfy_left_title",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "class" => "",
        "params" => array(
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Title", "careerfy-frame"),
                'param_name' => 'h_title',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button Text", "careerfy-frame"),
                'param_name' => 'btn_txt',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button URL", "careerfy-frame"),
                'param_name' => 'btn_url',
                'value' => '',
                'description' => ''
            ),
        )
    );

    if (function_exists('vc_map')) {
        vc_map($attributes);
    }
}

/**
 * Adding button shortcode
 * @return markup
 */
function careerfy_vc_button_shortcode() {

    $attributes = array(
        "name" => esc_html__("Button", "careerfy-frame"),
        "base" => "careerfy_button",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "class" => "",
        "params" => array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Style", "careerfy-frame"),
                'param_name' => 'btn_styl',
                'value' => array(
                    esc_html__("Style 1", "careerfy-frame") => 'view1',
                    esc_html__("Style 2", "careerfy-frame") => 'view2',
                ),
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button Text", "careerfy-frame"),
                'param_name' => 'btn_txt',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button URL", "careerfy-frame"),
                'param_name' => 'btn_url',
                'value' => '',
                'description' => ''
            ),
        )
    );

    if (function_exists('vc_map')) {
        vc_map($attributes);
    }
}

/**
 * Adding Advance Search shortcode
 * @return markup
 */
function careerfy_vc_advance_search() {

    $all_page = array();

    $args = array(
        'sort_order' => 'asc',
        'sort_column' => 'post_title',
        'hierarchical' => 1,
        'exclude' => '',
        'include' => '',
        'meta_key' => '',
        'meta_value' => '',
        'authors' => '',
        'child_of' => 0,
        'parent' => -1,
        'exclude_tree' => '',
        'number' => '',
        'offset' => 0,
        'post_type' => 'page',
        'post_status' => 'publish'
    );
    $pages = get_pages($args);
    if (!empty($pages)) {
        foreach ($pages as $page) {
            $all_page[$page->post_title] = $page->ID;
        }
    }

    $attributes = array(
        "name" => esc_html__("Advance Search", "careerfy-frame"),
        "base" => "careerfy_advance_search",
        "category" => esc_html__("Wp JobSearch", "careerfy-frame"),
        "class" => "",
        "params" => array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Style", "careerfy-frame"),
                'param_name' => 'view',
                'value' => array(
                    esc_html__("Style 1", "careerfy-frame") => 'view1',
                    esc_html__("Style 2", "careerfy-frame") => 'view2',
                    esc_html__("Style 3", "careerfy-frame") => 'view3',
                    esc_html__("Style 4", "careerfy-frame") => 'view4',
                    esc_html__("Style 5", "careerfy-frame") => 'view5',
                    esc_html__("Style 6", "careerfy-frame") => 'view6',
                    esc_html__("Style 7", "careerfy-frame") => 'view7',
                ),
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Title", "careerfy-frame"),
                'param_name' => 'srch_title',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textarea',
                'heading' => esc_html__("Description", "careerfy-frame"),
                'param_name' => 'srch_desc',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'careerfy_browse_img',
                'heading' => esc_html__("Background Image", "careerfy-frame"),
                'param_name' => 'srch_bg_img',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Search Result Page", "careerfy-frame"),
                'param_name' => 'result_page',
                'value' => $all_page,
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button 1 Text", "careerfy-frame"),
                'param_name' => 'btn1_txt',
                'value' => '',
                'description' => esc_html__("This will not show in Search Style 4.", "careerfy-frame"),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button 1 URL", "careerfy-frame"),
                'param_name' => 'btn1_url',
                'value' => '',
                'description' => esc_html__("This will not show in Search Style 4.", "careerfy-frame"),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button 2 Text", "careerfy-frame"),
                'param_name' => 'btn2_txt',
                'value' => '',
                'description' => esc_html__("This will only show in Search Style 1.", "careerfy-frame"),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button 2 URL", "careerfy-frame"),
                'param_name' => 'btn2_url',
                'value' => '',
                'description' => esc_html__("This will only show in Search Style 1.", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Keyword Field", "careerfy-frame"),
                'param_name' => 'keyword_field',
                'value' => array(
                    esc_html__("Show", "careerfy-frame") => 'show',
                    esc_html__("Hide", "careerfy-frame") => 'hide',
                ),
                'description' => '',
                'group' => esc_html__("Fields Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Location Field", "careerfy-frame"),
                'param_name' => 'location_field',
                'value' => array(
                    esc_html__("Show", "careerfy-frame") => 'show',
                    esc_html__("Hide", "careerfy-frame") => 'hide',
                ),
                'description' => '',
                'group' => esc_html__("Fields Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Sector Field", "careerfy-frame"),
                'param_name' => 'category_field',
                'value' => array(
                    esc_html__("Show", "careerfy-frame") => 'show',
                    esc_html__("Hide", "careerfy-frame") => 'hide',
                ),
                'description' => '',
                'group' => esc_html__("Fields Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__("Search Backround Color", "careerfy-frame"),
                'param_name' => 'search_bg_color',
                'value' => '',
                'description' => '',
                'group' => esc_html__("Color Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__("Title Color", "careerfy-frame"),
                'param_name' => 'search_title_color',
                'value' => '',
                'description' => '',
                'group' => esc_html__("Color Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__("Paragraph Color", "careerfy-frame"),
                'param_name' => 'search_paragraph_color',
                'value' => '',
                'description' => '',
                'group' => esc_html__("Color Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__("Link Color", "careerfy-frame"),
                'param_name' => 'search_link_color',
                'value' => '',
                'description' => '',
                'group' => esc_html__("Color Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__("Button Backround Color", "careerfy-frame"),
                'param_name' => 'search_btn_bg_color',
                'value' => '',
                'description' => '',
                'group' => esc_html__("Color Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__("Button Text Color", "careerfy-frame"),
                'param_name' => 'search_btn_txt_color',
                'value' => '',
                'description' => '',
                'group' => esc_html__("Color Settings", "careerfy-frame"),
            ),
        )
    );

    if (function_exists('vc_map') && class_exists('JobSearch_plugin')) {
        vc_map($attributes);
    }
}

/**
 * Adding job categories shortcode
 * @return markup
 */
function careerfy_vc_job_categories() {

    $all_page = array();

    $args = array(
        'sort_order' => 'asc',
        'sort_column' => 'post_title',
        'hierarchical' => 1,
        'exclude' => '',
        'include' => '',
        'meta_key' => '',
        'meta_value' => '',
        'authors' => '',
        'child_of' => 0,
        'parent' => -1,
        'exclude_tree' => '',
        'number' => '',
        'offset' => 0,
        'post_type' => 'page',
        'post_status' => 'publish'
    );
    $pages = get_pages($args);
    if (!empty($pages)) {
        foreach ($pages as $page) {
            $all_page[$page->post_title] = $page->ID;
        }
    }

    $attributes = array(
        "name" => esc_html__("Job Sectors", "careerfy-frame"),
        "base" => "careerfy_job_categories",
        "category" => esc_html__("Wp JobSearch", "careerfy-frame"),
        "class" => "",
        "params" => array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Style", "careerfy-frame"),
                'param_name' => 'cats_view',
                'value' => array(
                    esc_html__("Style 1", "careerfy-frame") => 'view1',
                    esc_html__("Style 2", "careerfy-frame") => 'view2',
                    esc_html__("Style 3", "careerfy-frame") => 'view3',
                    esc_html__("Style 4", "careerfy-frame") => 'view4',
                ),
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Number of Sectors", "careerfy-frame"),
                'param_name' => 'num_cats',
                'value' => '',
                'description' => esc_html__("Set number of Sectors to show", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Result Page", "careerfy-frame"),
                'param_name' => 'result_page',
                'value' => $all_page,
                'description' => ''
            ),
        )
    );

    if (function_exists('vc_map') && class_exists('JobSearch_plugin')) {
        vc_map($attributes);
    }
}

/**
 * Adding CV Packages shortcode
 * @return markup
 */
function careerfy_vc_cv_packages() {

    $all_pckgs = array(esc_html__("Select Package", "careerfy-frame") => '');

    $args = array(
        'post_type' => 'package',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'fields' => 'ids',
        'order' => 'ASC',
        'orderby' => 'title',
        'meta_query' => array(
            array(
                'key' => 'jobsearch_field_package_type',
                'value' => 'cv',
                'compare' => '=',
            ),
        ),
    );
    $pkgs_query = new WP_Query($args);

    if ($pkgs_query->found_posts > 0) {
        $pkgs_list = $pkgs_query->posts;

        if (!empty($pkgs_list)) {
            foreach ($pkgs_list as $pkg_item) {
                $cv_pkg_post = get_post($pkg_item);
                $cv_pkg_post_name = isset($cv_pkg_post->post_name) ? $cv_pkg_post->post_name : '';
                $all_pckgs[get_the_title($pkg_item)] = $cv_pkg_post_name;
            }
        }
    }

    $attributes = array(
        "name" => esc_html__("CV Packages", "careerfy-frame"),
        "base" => "careerfy_cv_packages",
        "category" => esc_html__("Wp JobSearch", "careerfy-frame"),
        "as_parent" => array('only' => 'careerfy_cv_package_item'),
        "content_element" => true,
        "show_settings_on_create" => false,
        "is_container" => true,
        "params" => array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("View", "careerfy-frame"),
                'param_name' => 'view',
                'value' => array(
                    esc_html__("View 1", "careerfy-frame") => 'view1',
                    esc_html__("View 2", "careerfy-frame") => 'view2',
                ),
                'description' => ''
            ),
        ),
        "js_view" => 'VcColumnView'
    );

    if (function_exists('vc_map') && class_exists('JobSearch_plugin')) {
        vc_map($attributes);
    }

    $attributes = array(
        "name" => esc_html__("Package Item", "careerfy-frame"),
        "base" => "careerfy_cv_package_item",
        "category" => esc_html__("Wp JobSearch", "careerfy-frame"),
        "content_element" => true,
        "as_child" => array('only' => 'careerfy_cv_packages'),
        "show_settings_on_create" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Select Package", "careerfy-frame"),
                'param_name' => 'att_pck',
                'value' => $all_pckgs,
                'description' => ''
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Featured", "careerfy-frame"),
                'param_name' => 'featured',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Sub Title", "careerfy-frame"),
                'param_name' => 'subtitle',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textarea',
                'heading' => esc_html__("Description", "careerfy-frame"),
                'param_name' => 'desc',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'param_group',
                'value' => '',
                'heading' => esc_html__("Features", "careerfy-frame"),
                'param_name' => 'pckg_features',
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __('Feature Name', 'careerfy-frame'),
                        'param_name' => 'feat_name',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__("Active", "careerfy-frame"),
                        'param_name' => 'feat_active',
                        'value' => array(
                            esc_html__("Yes", "careerfy-frame") => 'yes',
                            esc_html__("No", "careerfy-frame") => 'no',
                        ),
                        'description' => ''
                    ),
                ),
            ),
        ),
    );

    if (function_exists('vc_map') && class_exists('JobSearch_plugin')) {
        vc_map($attributes);
    }

    if (class_exists('WPBakeryShortCodesContainer') && class_exists('JobSearch_plugin')) {

        class WPBakeryShortCode_Careerfy_Cv_Packages extends WPBakeryShortCodesContainer {
            
        }

    }

    if (class_exists('WPBakeryShortCode') && class_exists('JobSearch_plugin')) {

        class WPBakeryShortCode_Careerfy_Cv_Package_Item extends WPBakeryShortCode {
            
        }

    }
}

/**
 * Adding Job Packages shortcode
 * @return markup
 */
function careerfy_vc_job_packages() {

    $all_pckgs = array(esc_html__("Select Package", "careerfy-frame") => '');

    $args = apply_filters('careerfy_job_pkgs_vcsh_args', array(
        'post_type' => 'package',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'fields' => 'ids',
        'order' => 'ASC',
        'orderby' => 'title',
        'meta_query' => array(
            array(
                'key' => 'jobsearch_field_package_type',
                'value' => 'job',
                'compare' => '=',
            ),
        ),
    ));
    $pkgs_query = new WP_Query($args);

    if ($pkgs_query->found_posts > 0) {
        $pkgs_list = $pkgs_query->posts;

        if (!empty($pkgs_list)) {
            foreach ($pkgs_list as $pkg_item) {
                $job_pkg_post = get_post($pkg_item);
                $job_pkg_post_name = isset($job_pkg_post->post_name) ? $job_pkg_post->post_name : '';
                $all_pckgs[get_the_title($pkg_item)] = $job_pkg_post_name;
            }
        }
    }

    $job_styles = array(
        esc_html__("Style 1", "careerfy-frame") => 'view1',
        esc_html__("Style 2", "careerfy-frame") => 'view2',
        esc_html__("Style 3", "careerfy-frame") => 'view3',
        esc_html__("Style 4", "careerfy-frame") => 'view4',
    );


    $job_styles = apply_filters('geek_finder_job_package_style', $job_styles);

    $job_compare_fields = array();
    $job_compare_fields = apply_filters('geek_finder_job_package_compare_fields', $job_compare_fields);

    $attributes = array(
        "name" => esc_html__("Job Packages", "careerfy-frame"),
        "base" => "careerfy_job_packages",
        "category" => esc_html__("Wp JobSearch", "careerfy-frame"),
        "as_parent" => array('only' => 'careerfy_job_package_item'),
        "content_element" => true,
        "show_settings_on_create" => false,
        "is_container" => true,
        "params" => array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Style", "careerfy-frame"),
                'param_name' => 'view',
                'value' => $job_styles,
                'description' => ''
            ),
        ),
        "js_view" => 'VcColumnView'
    );

    if (function_exists('vc_map') && class_exists('JobSearch_plugin')) {
        vc_map($attributes);
    }

    $attributes = array(
        "name" => esc_html__("Package Item", "careerfy-frame"),
        "base" => "careerfy_job_package_item",
        "category" => esc_html__("Wp JobSearch", "careerfy-frame"),
        "content_element" => true,
        "as_child" => array('only' => 'careerfy_job_packages'),
        "show_settings_on_create" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Select Package", "careerfy-frame"),
                'param_name' => 'att_pck',
                'value' => $all_pckgs,
                'description' => ''
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Featured", "careerfy-frame"),
                'param_name' => 'featured',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Duration", "careerfy-frame"),
                'param_name' => 'duration',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'param_group',
                'value' => '',
                'heading' => esc_html__("Features", "careerfy-frame"),
                'param_name' => 'pckg_features',
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __('Feature Name', 'careerfy-frame'),
                        'param_name' => 'feat_name',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__("Active", "careerfy-frame"),
                        'param_name' => 'feat_active',
                        'value' => array(
                            esc_html__("Yes", "careerfy-frame") => 'yes',
                            esc_html__("No", "careerfy-frame") => 'no',
                        ),
                        'description' => ''
                    ),
                ),
            ),
        ),
    );
    $attributes = apply_filters('geek_finder_job_package_compare_fields', $attributes);
    if (function_exists('vc_map') && class_exists('JobSearch_plugin')) {
        vc_map($attributes);
    }

    if (class_exists('WPBakeryShortCodesContainer') && class_exists('JobSearch_plugin')) {

        class WPBakeryShortCode_Careerfy_Job_Packages extends WPBakeryShortCodesContainer {
            
        }

    }

    if (class_exists('WPBakeryShortCode') && class_exists('JobSearch_plugin')) {

        class WPBakeryShortCode_Careerfy_Job_Package_Item extends WPBakeryShortCode {
            
        }

    }
}

/**
 * Adding All Packages shortcode
 * @return markup
 */
function careerfy_vc_all_packages() {

    $all_pckgs = array(esc_html__("Select Package", "careerfy-frame") => '');

    $args = apply_filters('careerfy_job_pkgs_vcsh_args', array(
        'post_type' => 'package',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'fields' => 'ids',
        'order' => 'ASC',
        'orderby' => 'title',
        'meta_query' => array(
            array(
                'key' => 'jobsearch_field_package_type',
                'value' => array('job', 'featured_jobs', 'cv', 'feature_job', 'candidate'),
                'compare' => 'IN',
            ),
        ),
    ));
    $pkgs_query = new WP_Query($args);
    if ($pkgs_query->found_posts > 0) {
        $pkgs_list = $pkgs_query->posts;
        if (!empty($pkgs_list)) {
            foreach ($pkgs_list as $pkg_item) {
                $job_pkg_post = get_post($pkg_item);
                $job_pkg_post_name = isset($job_pkg_post->post_name) ? $job_pkg_post->post_name : '';
                $all_pckgs[get_the_title($pkg_item)] = $job_pkg_post_name;
            }
        }
    }
    $job_styles = array(
        esc_html__("Style 1", "careerfy-frame") => 'view1',
        esc_html__("Style 2", "careerfy-frame") => 'view2',
        esc_html__("Style 3", "careerfy-frame") => 'view3',
        esc_html__("Style 4", "careerfy-frame") => 'view4',
        esc_html__("Style 5", "careerfy-frame") => 'view5',
        esc_html__("Style 6", "careerfy-frame") => 'view6',
        esc_html__("Style 7", "careerfy-frame") => 'view7',
        esc_html__("Style 8", "careerfy-frame") => 'view8',
        
    );

    $attributes = array(
        "name" => esc_html__("All Packages", "careerfy-frame"),
        "base" => "careerfy_all_packages",
        "category" => esc_html__("Wp JobSearch", "careerfy-frame"),
        "as_parent" => array('only' => 'careerfy_all_package_item'),
        "content_element" => true,
        "show_settings_on_create" => false,
        "is_container" => true,
        "params" => array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Style", "careerfy-frame"),
                'param_name' => 'view',
                'value' => $job_styles,
                'description' => ''
            ),
        ),
        "js_view" => 'VcColumnView'
    );

    if (function_exists('vc_map') && class_exists('JobSearch_plugin')) {
        vc_map($attributes);
    }

    $attributes = array(
        "name" => esc_html__("Package Item", "careerfy-frame"),
        "base" => "careerfy_all_package_item",
        "category" => esc_html__("Wp JobSearch", "careerfy-frame"),
        "content_element" => true,
        "as_child" => array('only' => 'careerfy_all_packages'),
        "show_settings_on_create" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Select Package", "careerfy-frame"),
                'param_name' => 'att_pck',
                'value' => $all_pckgs,
                'description' => ''
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Featured", "careerfy-frame"),
                'param_name' => 'featured',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Duration", "careerfy-frame"),
                'param_name' => 'duration',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textarea',
                'heading' => esc_html__("Description", "careerfy-frame"),
                'param_name' => 'desc',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'param_group',
                'value' => '',
                'heading' => esc_html__("Features", "careerfy-frame"),
                'param_name' => 'pckg_features',
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __('Feature Name', 'careerfy-frame'),
                        'param_name' => 'feat_name',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__("Active", "careerfy-frame"),
                        'param_name' => 'feat_active',
                        'value' => array(
                            esc_html__("Yes", "careerfy-frame") => 'yes',
                            esc_html__("No", "careerfy-frame") => 'no',
                        ),
                        'description' => ''
                    ),
                ),
            ),
        ),
    );
    $attributes = apply_filters('geek_finder_job_package_compare_fields', $attributes);
    if (function_exists('vc_map') && class_exists('JobSearch_plugin')) {
        vc_map($attributes);
    }

    if (class_exists('WPBakeryShortCodesContainer') && class_exists('JobSearch_plugin')) {

        class WPBakeryShortCode_Careerfy_All_Packages extends WPBakeryShortCodesContainer {
            
        }

    }

    if (class_exists('WPBakeryShortCode') && class_exists('JobSearch_plugin')) {

        class WPBakeryShortCode_Careerfy_All_Package_Item extends WPBakeryShortCode {
            
        }

    }
}

/**
 * Adding Candidate Applications Packages shortcode
 * @return markup
 */
function careerfy_vc_candidate_packages() {

    $all_pckgs = array(esc_html__("Select Package", "careerfy-frame") => '');

    $args = array(
        'post_type' => 'package',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'fields' => 'ids',
        'order' => 'ASC',
        'orderby' => 'title',
        'meta_query' => array(
            array(
                'key' => 'jobsearch_field_package_type',
                'value' => 'candidate',
                'compare' => '=',
            ),
        ),
    );
    $pkgs_query = new WP_Query($args);

    if ($pkgs_query->found_posts > 0) {
        $pkgs_list = $pkgs_query->posts;

        if (!empty($pkgs_list)) {
            foreach ($pkgs_list as $pkg_item) {
                $_pkg_post = get_post($pkg_item);
                $_pkg_post_name = isset($_pkg_post->post_name) ? $_pkg_post->post_name : '';
                $all_pckgs[get_the_title($pkg_item)] = $_pkg_post_name;
            }
        }
    }

    $attributes = array(
        "name" => esc_html__("Candidate Packages", "careerfy-frame"),
        "base" => "careerfy_candidate_packages",
        "category" => esc_html__("Wp JobSearch", "careerfy-frame"),
        "as_parent" => array('only' => 'careerfy_candidate_package_item'),
        "content_element" => true,
        "show_settings_on_create" => false,
        "is_container" => true,
        "params" => apply_filters('careerfy_candidate_pkgs_vcsh_params', array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("View", "careerfy-frame"),
                'param_name' => 'view',
                'value' => array(
                    esc_html__("View 1", "careerfy-frame") => 'view1',
                ),
                'description' => ''
            ),
        )),
        "js_view" => 'VcColumnView'
    );

    if (function_exists('vc_map') && class_exists('JobSearch_plugin')) {
        vc_map($attributes);
    }

    $attributes = array(
        "name" => esc_html__("Package Item", "careerfy-frame"),
        "base" => "careerfy_candidate_package_item",
        "category" => esc_html__("Wp JobSearch", "careerfy-frame"),
        "content_element" => true,
        "as_child" => array('only' => 'careerfy_candidate_packages'),
        "show_settings_on_create" => true,
        "params" => apply_filters('careerfy_candidate_pkg_item_vcsh_params', array(
            // add params same as with any other content element
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Select Package", "careerfy-frame"),
                'param_name' => 'att_pck',
                'value' => $all_pckgs,
                'description' => ''
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Featured", "careerfy-frame"),
                'param_name' => 'featured',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Sub Title", "careerfy-frame"),
                'param_name' => 'subtitle',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textarea',
                'heading' => esc_html__("Description", "careerfy-frame"),
                'param_name' => 'desc',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'param_group',
                'value' => '',
                'heading' => esc_html__("Features", "careerfy-frame"),
                'param_name' => 'pckg_features',
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => __('Feature Name', 'careerfy-frame'),
                        'param_name' => 'feat_name',
                    ),
                    array(
                        'type' => 'dropdown',
                        'heading' => esc_html__("Active", "careerfy-frame"),
                        'param_name' => 'feat_active',
                        'value' => array(
                            esc_html__("Yes", "careerfy-frame") => 'yes',
                            esc_html__("No", "careerfy-frame") => 'no',
                        ),
                        'description' => ''
                    ),
                ),
            ),
        )),
    );

    if (function_exists('vc_map') && class_exists('JobSearch_plugin')) {
        vc_map($attributes);
    }

    if (class_exists('WPBakeryShortCodesContainer') && class_exists('JobSearch_plugin')) {

        class WPBakeryShortCode_Careerfy_Candidate_Packages extends WPBakeryShortCodesContainer {
            
        }

    }

    if (class_exists('WPBakeryShortCode') && class_exists('JobSearch_plugin')) {

        class WPBakeryShortCode_Careerfy_Candidate_Package_Item extends WPBakeryShortCode {
            
        }

    }
}

/**
 * adding jobs listing shortcode
 * @return markup
 */
function careerfy_vc_jobs_listing() {

    $categories = get_terms(array(
        'taxonomy' => 'sector',
        'hide_empty' => false,
    ));

    $cate_array = array(esc_html__("Select Sector", "careerfy-frame") => '');
    if (is_array($categories) && sizeof($categories) > 0) {
        foreach ($categories as $category) {
            $cate_array[$category->name] = $category->slug;
        }
    }

    $attributes = array(
        "name" => esc_html__("Jobs Listing", "careerfy-frame"),
        "base" => "jobsearch_job_shortcode",
        "class" => "",
        "category" => esc_html__("Wp JobSearch", "careerfy-frame"),
        "params" => apply_filters('jobsearch_job_listings_vcsh_params', array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("View", "careerfy-frame"),
                'param_name' => 'job_view',
                'value' => array(
                    esc_html__("Style 1", "careerfy-frame") => 'view-default',
                    esc_html__("Style 2", "careerfy-frame") => 'view-medium',
                    esc_html__("Style 3", "careerfy-frame") => 'view-listing2',
                    esc_html__("Style 4", "careerfy-frame") => 'view-grid2',
                    esc_html__("Style 5", "careerfy-frame") => 'view-medium2',
                    esc_html__("Style 6", "careerfy-frame") => 'view-grid',
                    esc_html__("Style 7", "careerfy-frame") => 'view-medium3',
                ),
                'description' => esc_html__("Select jobs listing view.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Sector", "careerfy-frame"),
                'param_name' => 'job_cat',
                'value' => $cate_array,
                'description' => esc_html__("Select Sector.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Featured Only", "careerfy-frame"),
                'param_name' => 'featured_only',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => esc_html__("If you set Featured Only 'Yes' then only Featured jobs will show.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Filters", "careerfy-frame"),
                'param_name' => 'job_filters',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Jobs searching filters switch.", "careerfy-frame"),
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Filters Count", "careerfy-frame"),
                'param_name' => 'job_filters_count',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => esc_html__("Show result counts in front of every filter.", "careerfy-frame"),
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Locations", "careerfy-frame"),
                'param_name' => 'job_filters_loc',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Jobs searching filters 'Location' switch.", "careerfy-frame"),
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Locations Filter Collapse", "careerfy-frame"),
                'param_name' => 'job_filters_loc_collapse',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => '',
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Locations Filter Style", "careerfy-frame"),
                'param_name' => 'job_filters_loc_view',
                'value' => array(
                    esc_html__("Checkbox List", "careerfy-frame") => 'checkboxes',
                    esc_html__("Input Field", "careerfy-frame") => 'input',
                ),
                'description' => '',
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Date Posted", "careerfy-frame"),
                'param_name' => 'job_filters_date',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Jobs searching filters 'Date Posted' switch.", "careerfy-frame"),
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Date Posted Filter Collapse", "careerfy-frame"),
                'param_name' => 'job_filters_date_collapse',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => '',
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Job Type", "careerfy-frame"),
                'param_name' => 'job_filters_type',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Jobs searching filters 'Job Type' switch.", "careerfy-frame"),
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Job Type Filter Collapse", "careerfy-frame"),
                'param_name' => 'job_filters_type_collapse',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => '',
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Sector", "careerfy-frame"),
                'param_name' => 'job_filters_sector',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Jobs searching filters 'Sector' switch.", "careerfy-frame"),
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Sector Filter Collapse", "careerfy-frame"),
                'param_name' => 'job_filters_sector_collapse',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => '',
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Top Map", "careerfy-frame"),
                'param_name' => 'job_top_map',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => esc_html__("Jobs top map switch.", "careerfy-frame"),
                'group' => esc_html__("Map Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Map Height", "careerfy-frame"),
                'param_name' => 'job_top_map_height',
                'value' => '450',
                'description' => esc_html__("Jobs top map height.", "careerfy-frame"),
                'group' => esc_html__("Map Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Map Zoom", "careerfy-frame"),
                'param_name' => 'job_top_map_zoom',
                'value' => '8',
                'description' => esc_html__("Jobs top map zoom.", "careerfy-frame"),
                'group' => esc_html__("Map Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Featured Jobs on Top", "careerfy-frame"),
                'param_name' => 'job_feat_jobs_top',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => esc_html__("Featured jobs will display on top of listing.", "careerfy-frame"),
                'group' => esc_html__("Featured Jobs", "careerfy-frame"),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Number of Featured jobs", "careerfy-frame"),
                'param_name' => 'num_of_feat_jobs',
                'value' => '5',
                'description' => '',
                'group' => esc_html__("Featured Jobs", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Top Search Bar", "careerfy-frame"),
                'param_name' => 'job_top_search',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Jobs top search bar switch.", "careerfy-frame"),
                'group' => esc_html__("Top Search", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Top Search Style", "careerfy-frame"),
                'param_name' => 'job_top_search_view',
                'value' => array(
                    esc_html__("Simple", "careerfy-frame") => 'simple',
                    esc_html__("Advance Search", "careerfy-frame") => 'advance',
                ),
                'description' => esc_html__("Jobs top search style.", "careerfy-frame"),
                'group' => esc_html__("Top Search", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("AutoFill Search Box", "careerfy-frame"),
                'param_name' => 'top_search_autofill',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Enable/Disable autofill in search keyword field.", "careerfy-frame"),
                'group' => esc_html__("Top Search", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Sort by Fields", "careerfy-frame"),
                'param_name' => 'job_sort_by',
                'value' => array(
                    esc_html__("Yes", "jobsearch-vc") => 'yes',
                    esc_html__("No", "jobsearch-vc") => 'no',
                ),
                'description' => esc_html__("Results search sorting section switch.", "careerfy-frame")
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Excerpt Length", "careerfy-frame"),
                'param_name' => 'job_excerpt',
                'value' => '20',
                'description' => esc_html__("Set number of words you want show for excerpt.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Order", "careerfy-frame"),
                'param_name' => 'job_order',
                'value' => array(
                    esc_html__("Descending", "careerfy-frame") => 'DESC',
                    esc_html__("Ascending", "careerfy-frame") => 'ASC',
                ),
                'description' => esc_html__("Choose job list items order.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Orderby", "careerfy-frame"),
                'param_name' => 'job_orderby',
                'value' => array(
                    esc_html__("Date", "careerfy-frame") => 'date',
                    esc_html__("Title", "careerfy-frame") => 'title',
                ),
                'description' => esc_html__("Choose job list items orderby.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Pagination", "careerfy-frame"),
                'param_name' => 'job_pagination',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Choose yes if you want to show pagination for job items.", "careerfy-frame")
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Jobs per Page", "careerfy-frame"),
                'param_name' => 'job_per_page',
                'value' => '10',
                'description' => esc_html__("Set number that how much jobs you want to show per page. Leave it blank for all jobs on a single page.", "careerfy-frame")
            )
        ))
    );

    if (function_exists('vc_map') && class_exists('JobSearch_plugin')) {
        vc_map($attributes);
    }
}

/**
 * adding employer listing shortcode
 * @return markup
 */
function careerfy_vc_employer_listing() {

    $categories = get_terms(array(
        'taxonomy' => 'sector',
        'hide_empty' => false,
    ));

    $cate_array = array(esc_html__("Select Sector", "careerfy-frame") => '');
    if (is_array($categories) && sizeof($categories) > 0) {
        foreach ($categories as $category) {
            $cate_array[$category->name] = $category->slug;
        }
    }

    $attributes = array(
        "name" => esc_html__("Employer Listing", "careerfy-frame"),
        "base" => "jobsearch_employer_shortcode",
        "class" => "",
        "category" => esc_html__("Wp JobSearch", "careerfy-frame"),
        "params" => apply_filters('jobsearch_employer_listings_vcsh_params', array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("View", "careerfy-frame"),
                'param_name' => 'employer_view',
                'value' => array(
                    esc_html__("Style 1", "careerfy-frame") => 'view-default',
                    esc_html__("Style 2", "careerfy-frame") => 'view-grid',
                    esc_html__("Style 3", "careerfy-frame") => 'view-slider',
                ),
                'description' => esc_html__("Select employers listing view.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Sector", "careerfy-frame"),
                'param_name' => 'employer_cat',
                'value' => $cate_array,
                'description' => esc_html__("Select Sector.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Filters", "careerfy-frame"),
                'param_name' => 'employer_filters',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Employers searching filters switch.", "careerfy-frame"),
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Filters Count", "careerfy-frame"),
                'param_name' => 'employer_filters_count',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => esc_html__("Show result counts in front of every filter.", "careerfy-frame"),
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Locations", "careerfy-frame"),
                'param_name' => 'employer_filters_loc',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Employers searching filters 'Location' switch.", "careerfy-frame"),
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Locations Filter Collapse", "careerfy-frame"),
                'param_name' => 'employer_filters_loc_collapse',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => '',
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Locations Filter Style", "careerfy-frame"),
                'param_name' => 'employer_filters_loc_view',
                'value' => array(
                    esc_html__("Checkbox List", "careerfy-frame") => 'checkboxes',
                    esc_html__("Input Field", "careerfy-frame") => 'input',
                ),
                'description' => '',
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Date Posted", "careerfy-frame"),
                'param_name' => 'employer_filters_date',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Employers searching filters 'Date Posted' switch.", "careerfy-frame"),
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Date Posted Collapse", "careerfy-frame"),
                'param_name' => 'employer_filters_date_collapse',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => '',
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Sector", "careerfy-frame"),
                'param_name' => 'employer_filters_sector',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Employers searching filters 'Sector' switch.", "careerfy-frame"),
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Sector Filter Collapse", "careerfy-frame"),
                'param_name' => 'employer_filters_sector_collapse',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => '',
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Team Size", "careerfy-frame"),
                'param_name' => 'employer_filters_team',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Employers searching filters 'Team Size' switch.", "careerfy-frame"),
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Team Size Filter Collapse", "careerfy-frame"),
                'param_name' => 'employer_filters_team_collapse',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => '',
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Top Map", "careerfy-frame"),
                'param_name' => 'emp_top_map',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => esc_html__("Employers top map switch.", "careerfy-frame"),
                'group' => esc_html__("Map Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Map Height", "careerfy-frame"),
                'param_name' => 'emp_top_map_height',
                'value' => '450',
                'description' => esc_html__("Employers top map height.", "careerfy-frame"),
                'group' => esc_html__("Map Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Map Zoom", "careerfy-frame"),
                'param_name' => 'emp_top_map_zoom',
                'value' => '8',
                'description' => esc_html__("Employers top map zoom.", "careerfy-frame"),
                'group' => esc_html__("Map Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Top Search Bar", "careerfy-frame"),
                'param_name' => 'emp_top_search',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => esc_html__("Employers top search bar switch.", "careerfy-frame"),
                'group' => esc_html__("Top Search", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Top Search Style", "careerfy-frame"),
                'param_name' => 'emp_top_search_view',
                'value' => array(
                    esc_html__("Simple", "careerfy-frame") => 'simple',
                    esc_html__("Advance Search", "careerfy-frame") => 'advance',
                ),
                'description' => esc_html__("Employers top search style.", "careerfy-frame"),
                'group' => esc_html__("Top Search", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("AutoFill Search Box", "careerfy-frame"),
                'param_name' => 'top_search_autofill',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Enable/Disable autofill in search keyword field.", "careerfy-frame"),
                'group' => esc_html__("Top Search", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Sort by Fields", "careerfy-frame"),
                'param_name' => 'employer_sort_by',
                'value' => array(
                    esc_html__("Yes", "jobsearch-vc") => 'yes',
                    esc_html__("No", "jobsearch-vc") => 'no',
                ),
                'description' => esc_html__("Results search sorting section switch.", "careerfy-frame")
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Excerpt Length", "careerfy-frame"),
                'param_name' => 'employer_excerpt',
                'value' => '20',
                'description' => esc_html__("Set number of words you want show for excerpt.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Order", "careerfy-frame"),
                'param_name' => 'employer_order',
                'value' => array(
                    esc_html__("Descending", "careerfy-frame") => 'DESC',
                    esc_html__("Ascending", "careerfy-frame") => 'ASC',
                ),
                'description' => esc_html__("Choose job list items order.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Orderby", "careerfy-frame"),
                'param_name' => 'employer_orderby',
                'value' => array(
                    esc_html__("Date", "careerfy-frame") => 'date',
                    esc_html__("Title", "careerfy-frame") => 'title',
                ),
                'description' => esc_html__("Choose list items orderby.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Pagination", "careerfy-frame"),
                'param_name' => 'employer_pagination',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Choose yes if you want to show pagination for employer items.", "careerfy-frame")
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Items per Page", "careerfy-frame"),
                'param_name' => 'employer_per_page',
                'value' => '10',
                'description' => esc_html__("Set number that how much employers you want to show per page. Leave it blank for all employers on a single page.", "careerfy-frame")
            )
        ))
    );

    if (function_exists('vc_map') && class_exists('JobSearch_plugin')) {
        vc_map($attributes);
    }
}

/**
 * adding candidate listing shortcode
 * @return markup
 */
function careerfy_vc_candidate_listing() {

    $categories = get_terms(array(
        'taxonomy' => 'sector',
        'hide_empty' => false,
    ));

    $cate_array = array(esc_html__("Select Sector", "careerfy-frame") => '');
    if (is_array($categories) && sizeof($categories) > 0) {
        foreach ($categories as $category) {
            $cate_array[$category->name] = $category->slug;
        }
    }

    $attributes = array(
        "name" => esc_html__("Candidate Listing", "careerfy-frame"),
        "base" => "jobsearch_candidate_shortcode",
        "class" => "",
        "category" => esc_html__("Wp JobSearch", "careerfy-frame"),
        "params" => apply_filters('jobsearch_candidate_listings_vcsh_params', array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("View", "careerfy-frame"),
                'param_name' => 'candidate_view',
                'value' => array(
                    esc_html__("Style 1", "careerfy-frame") => 'view-default',
                    esc_html__("Style 2", "careerfy-frame") => 'view-grid',
                    esc_html__("Style 3", "careerfy-frame") => 'view-classic',
                    esc_html__("Style 4", "careerfy-frame") => 'view-modern',
                ),
                'description' => esc_html__("Select candidates listing view.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Sector", "careerfy-frame"),
                'param_name' => 'candidate_cat',
                'value' => $cate_array,
                'description' => esc_html__("Select Sector.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Filters", "careerfy-frame"),
                'param_name' => 'candidate_filters',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Candidates searching filters switch.", "careerfy-frame"),
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Filters Count", "careerfy-frame"),
                'param_name' => 'candidate_filters_count',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => esc_html__("Show result counts in front of every filter.", "careerfy-frame"),
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Date Posted", "careerfy-frame"),
                'param_name' => 'candidate_filters_date',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Candidates searching filters 'Date Posted' switch.", "careerfy-frame"),
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Date Posted Collapse", "careerfy-frame"),
                'param_name' => 'candidate_filters_date_collapse',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => '',
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Sector", "careerfy-frame"),
                'param_name' => 'candidate_filters_sector',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Candidates searching filters 'Sector' switch.", "careerfy-frame"),
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Sector Filter Collapse", "careerfy-frame"),
                'param_name' => 'candidate_filters_sector_collapse',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => '',
                'group' => esc_html__("Filters Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Top Map", "careerfy-frame"),
                'param_name' => 'cand_top_map',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => esc_html__("Candidates top map switch.", "careerfy-frame"),
                'group' => esc_html__("Map Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Map Height", "careerfy-frame"),
                'param_name' => 'cand_top_map_height',
                'value' => '450',
                'description' => esc_html__("Candidates top map height.", "careerfy-frame"),
                'group' => esc_html__("Map Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Map Zoom", "careerfy-frame"),
                'param_name' => 'cand_top_map_zoom',
                'value' => '8',
                'description' => esc_html__("Candidates top map zoom.", "careerfy-frame"),
                'group' => esc_html__("Map Settings", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Top Search Bar", "careerfy-frame"),
                'param_name' => 'cand_top_search',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'description' => esc_html__("Candidates top search bar switch.", "careerfy-frame"),
                'group' => esc_html__("Top Search", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Top Search Style", "careerfy-frame"),
                'param_name' => 'cand_top_search_view',
                'value' => array(
                    esc_html__("Simple", "careerfy-frame") => 'simple',
                    esc_html__("Advance Search", "careerfy-frame") => 'advance',
                ),
                'description' => esc_html__("Candidates top search style.", "careerfy-frame"),
                'group' => esc_html__("Top Search", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("AutoFill Search Box", "careerfy-frame"),
                'param_name' => 'top_search_autofill',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Enable/Disable autofill in search keyword field.", "careerfy-frame"),
                'group' => esc_html__("Top Search", "careerfy-frame"),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Sort by Fields", "careerfy-frame"),
                'param_name' => 'candidate_sort_by',
                'value' => array(
                    esc_html__("Yes", "jobsearch-vc") => 'yes',
                    esc_html__("No", "jobsearch-vc") => 'no',
                ),
                'description' => esc_html__("Results search sorting section switch.", "careerfy-frame")
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Excerpt Length", "careerfy-frame"),
                'param_name' => 'candidate_excerpt',
                'value' => '20',
                'description' => esc_html__("Set number of words you want show for excerpt.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Order", "careerfy-frame"),
                'param_name' => 'candidate_order',
                'value' => array(
                    esc_html__("Descending", "careerfy-frame") => 'DESC',
                    esc_html__("Ascending", "careerfy-frame") => 'ASC',
                ),
                'description' => esc_html__("Choose candidate list items order.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Orderby", "careerfy-frame"),
                'param_name' => 'candidate_orderby',
                'value' => array(
                    esc_html__("Date", "careerfy-frame") => 'date',
                    esc_html__("Title", "careerfy-frame") => 'title',
                ),
                'description' => esc_html__("Choose list items orderby.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Pagination", "careerfy-frame"),
                'param_name' => 'candidate_pagination',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => esc_html__("Choose yes if you want to show pagination for candidate items.", "careerfy-frame")
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Items per Page", "careerfy-frame"),
                'param_name' => 'candidate_per_page',
                'value' => '10',
                'description' => esc_html__("Set number that how much candidates you want to show per page. Leave it blank for all candidates on a single page.", "careerfy-frame")
            )
        ))
    );

    if (function_exists('vc_map') && class_exists('JobSearch_plugin')) {
        vc_map($attributes);
    }
}

/**
 * Adding call to action shortcode
 * @return markup
 */
function careerfy_vc_call_to_action() {

    $attributes = array(
        "name" => esc_html__("Call to Action", "careerfy-frame"),
        "base" => "careerfy_call_to_action",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "class" => "",
        "params" => array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("View", "careerfy-frame"),
                'param_name' => 'view',
                'value' => array(
                    esc_html__("View 1", "careerfy-frame") => 'view-1',
                    esc_html__("View 2", "careerfy-frame") => 'view-2',
                ),
                'description' => ''
            ),
            array(
                'type' => 'careerfy_browse_img',
                'heading' => esc_html__("Image", "careerfy-frame"),
                'param_name' => 'cta_img',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Title 1", "careerfy-frame"),
                'param_name' => 'cta_title1',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Title 2", "careerfy-frame"),
                'param_name' => 'cta_title2',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textarea',
                'heading' => esc_html__("Description", "careerfy-frame"),
                'param_name' => 'cta_desc',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button Text", "careerfy-frame"),
                'param_name' => 'btn_txt',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button URL", "careerfy-frame"),
                'param_name' => 'btn_url',
                'value' => '',
                'description' => ''
            ),
        )
    );

    if (function_exists('vc_map')) {
        vc_map($attributes);
    }
}

/**
 * Adding About Company shortcode
 * @return markup
 */
function careerfy_vc_about_company() {

    $attributes = array(
        "name" => esc_html__("About Company", "careerfy-frame"),
        "base" => "careerfy_about_company",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "class" => "",
        "params" => array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Style", "careerfy-frame"),
                'param_name' => 'ab_view',
                'value' => array(
                    esc_html__("Style 1", "careerfy-frame") => 'view1',
                    esc_html__("Style 2", "careerfy-frame") => 'view2',
                ),
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Title", "careerfy-frame"),
                'param_name' => 'title',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textarea',
                'heading' => esc_html__("Bold Text", "careerfy-frame"),
                'param_name' => 'bold_txt',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textarea_html',
                'heading' => esc_html__("About Text", "careerfy-frame"),
                'param_name' => 'content',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__("Title Color", "careerfy-frame"),
                'param_name' => 'title_color',
                'value' => '',
                'description' => '',
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__("Content Color", "careerfy-frame"),
                'param_name' => 'desc_color',
                'value' => '',
                'description' => '',
            ),
            array(
                'type' => 'careerfy_browse_img',
                'heading' => esc_html__("Image", "careerfy-frame"),
                'param_name' => 'about_img',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button Text", "careerfy-frame"),
                'param_name' => 'btn_txt',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button URL", "careerfy-frame"),
                'param_name' => 'btn_url',
                'value' => '',
                'description' => ''
            ),
        )
    );

    if (function_exists('vc_map')) {
        vc_map($attributes);
    }
}

/**
 * Adding Block Text Box shortcode
 * @return markup
 */
function careerfy_vc_block_text_box() {

    $attributes = array(
        "name" => esc_html__("Block Text with Video", "careerfy-frame"),
        "base" => "careerfy_block_text_box",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "class" => "",
        "params" => array(
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Title", "careerfy-frame"),
                'param_name' => 'title',
                'value' => '',
                'description' => '',
                'group' => esc_html__("Block Text", "careerfy-frame"),
            ),
            array(
                'type' => 'textarea_html',
                'heading' => esc_html__("Text", "careerfy-frame"),
                'param_name' => 'content',
                'value' => '',
                'description' => '',
                'group' => esc_html__("Block Text", "careerfy-frame"),
            ),
            array(
                'type' => 'careerfy_browse_img',
                'heading' => esc_html__("Background Image", "careerfy-frame"),
                'param_name' => 'bg_img',
                'value' => '',
                'description' => '',
                'group' => esc_html__("Block Text", "careerfy-frame"),
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__("Background Color", "careerfy-frame"),
                'param_name' => 'bg_color',
                'value' => '',
                'description' => '',
                'group' => esc_html__("Block Text", "careerfy-frame"),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button Text", "careerfy-frame"),
                'param_name' => 'btn_txt',
                'value' => '',
                'description' => '',
                'group' => esc_html__("Block Text", "careerfy-frame"),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button URL", "careerfy-frame"),
                'param_name' => 'btn_url',
                'value' => '',
                'description' => '',
                'group' => esc_html__("Block Text", "careerfy-frame"),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Video URL", "careerfy-frame"),
                'param_name' => 'video_url',
                'value' => '',
                'description' => '',
                'group' => esc_html__("Video", "careerfy-frame"),
            ),
            array(
                'type' => 'careerfy_browse_img',
                'heading' => esc_html__("Video Poster Image", "careerfy-frame"),
                'param_name' => 'poster_img',
                'value' => '',
                'description' => '',
                'group' => esc_html__("Video", "careerfy-frame"),
            ),
        )
    );

    if (function_exists('vc_map')) {
        vc_map($attributes);
    }
}

/**
 * Adding Simple Block Text shortcode
 * @return markup
 */
function careerfy_vc_simple_block_text() {

    $attributes = array(
        "name" => esc_html__("Simple Block Text", "careerfy-frame"),
        "base" => "careerfy_simple_block_text",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "class" => "",
        "params" => array(
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Title", "careerfy-frame"),
                'param_name' => 'title',
                'value' => '',
                'description' => '',
            ),
            array(
                'type' => 'textarea_html',
                'heading' => esc_html__("Content Text", "careerfy-frame"),
                'param_name' => 'content',
                'value' => '',
                'description' => '',
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__("Title Color", "careerfy-frame"),
                'param_name' => 'title_color',
                'value' => '',
                'description' => '',
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__("Content Color", "careerfy-frame"),
                'param_name' => 'desc_color',
                'value' => '',
                'description' => '',
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button 1 Text", "careerfy-frame"),
                'param_name' => 'btn_txt',
                'value' => '',
                'description' => '',
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button 1 URL", "careerfy-frame"),
                'param_name' => 'btn_url',
                'value' => '',
                'description' => '',
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button 2 Text", "careerfy-frame"),
                'param_name' => 'btn2_txt',
                'value' => '',
                'description' => '',
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button 2 URL", "careerfy-frame"),
                'param_name' => 'btn2_url',
                'value' => '',
                'description' => '',
            ),
        )
    );

    if (function_exists('vc_map')) {
        vc_map($attributes);
    }
}

/**
 * Adding Google Map shortcode
 * @return markup
 */
function careerfy_vc_google_map_shortcode() {

    $attributes = array(
        "name" => esc_html__("Google Map", "careerfy-frame"),
        "base" => "careerfy_google_map",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "class" => "",
        "params" => array(
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Latitude", "careerfy-frame"),
                'param_name' => 'map_latitude',
                'value' => '51.2',
                'description' => esc_html__("Set Latitude of map.", "careerfy-frame"),
                'group' => __('Map Settings', 'careerfy-frame'),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Longitude", "careerfy-frame"),
                'param_name' => 'map_longitude',
                'value' => '0.2',
                'description' => esc_html__("Set Longitude of map.", "careerfy-frame"),
                'group' => __('Map Settings', 'careerfy-frame'),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Zoom", "careerfy-frame"),
                'param_name' => 'map_zoom',
                'value' => '8',
                'description' => esc_html__("Set Zoom for map.", "careerfy-frame"),
                'group' => __('Map Settings', 'careerfy-frame'),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Height", "careerfy-frame"),
                'param_name' => 'map_height',
                'value' => '350',
                'description' => esc_html__("Set Height for map.", "careerfy-frame"),
                'group' => __('Map Settings', 'careerfy-frame'),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Street View", "careerfy-frame"),
                'param_name' => 'map_street_view',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => '',
                'group' => __('Map Settings', 'careerfy-frame'),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Scroll Wheel control", "careerfy-frame"),
                'param_name' => 'map_scrollwheel',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => '',
                'group' => __('Map Settings', 'careerfy-frame'),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Disable Map Type", "careerfy-frame"),
                'param_name' => 'map_default_ui',
                'value' => array(
                    esc_html__("No", "careerfy-frame") => 'no',
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                ),
                'group' => __('Map Settings', 'careerfy-frame'),
                'description' => ''
            ),
            array(
                'type' => 'nimble_browse_img',
                'heading' => esc_html__("Marker Icon", "careerfy-frame"),
                'param_name' => 'map_marker',
                'value' => '',
                'description' => esc_html__("Put custom marker icon for map.", "careerfy-frame"),
                'group' => __('Map Settings', 'careerfy-frame'),
            ),
            array(
                'type' => 'textarea',
                'heading' => esc_html__("Styles", "careerfy-frame"),
                'param_name' => 'map_styles',
                'value' => '',
                'description' => __("Set map styles. You can get predefined styles from <a href=\"https://snazzymaps.com/\" target=\"_blank\">snazzymaps.com</a>", "careerfy-frame"),
                'group' => __('Map Settings', 'careerfy-frame'),
            ),
        )
    );

    if (function_exists('vc_map')) {
        vc_map($attributes);
    }
}

/**
 * Adding Contact information shortcode
 * @return markup
 */
function careerfy_vc_contact_information() {

    $cf7_posts = get_posts(array(
        'post_type' => 'wpcf7_contact_form',
        'numberposts' => -1
    ));
    $cf7_arr = array(
        esc_html__("Select Form", "careerfy-frame") => ''
    );
    if (!empty($cf7_posts)) {
        foreach ($cf7_posts as $p) {
            $cf7_arr[$p->post_title] = $p->post_name;
        }
    }

    $params = array();
    $params[] = array(
        'type' => 'textfield',
        'heading' => esc_html__("Contact Info title", "careerfy-frame"),
        'param_name' => 'con_info_title',
        'value' => '',
        'description' => '',
    );
    if (class_exists('WPCF7_ContactForm')) {
        $params[] = array(
            'type' => 'dropdown',
            'heading' => esc_html__("Select Contact Form 7", "careerfy-frame"),
            'param_name' => 'con_form_7',
            'value' => $cf7_arr,
            'description' => '',
        );
    }
    $params[] = array(
        'type' => 'textfield',
        'heading' => esc_html__("Contact Form title", "careerfy-frame"),
        'param_name' => 'con_form_title',
        'value' => '',
        'description' => '',
    );
    $params[] = array(
        'type' => 'textarea',
        'heading' => esc_html__("Description", "careerfy-frame"),
        'param_name' => 'con_desc',
        'value' => '',
        'description' => '',
    );
    $params[] = array(
        'type' => 'textfield',
        'heading' => esc_html__("Address", "careerfy-frame"),
        'param_name' => 'con_address',
        'value' => '',
        'description' => '',
    );
    $params[] = array(
        'type' => 'textfield',
        'heading' => esc_html__("Email", "careerfy-frame"),
        'param_name' => 'con_email',
        'value' => '',
        'description' => '',
    );
    $params[] = array(
        'type' => 'textfield',
        'heading' => esc_html__("Phone", "careerfy-frame"),
        'param_name' => 'con_phone',
        'value' => '',
        'description' => '',
    );
    $params[] = array(
        'type' => 'textfield',
        'heading' => esc_html__("Fax", "careerfy-frame"),
        'param_name' => 'con_fax',
        'value' => '',
        'description' => '',
    );
    $params[] = array(
        'type' => 'param_group',
        'value' => '',
        'heading' => esc_html__("Social Links", "careerfy-frame"),
        'param_name' => 'social_links',
        'params' => array(
            array(
                'type' => 'iconpicker',
                'value' => '',
                'heading' => __('Social Icon', 'careerfy-frame'),
                'param_name' => 'soc_icon',
            ),
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __('Social Link', 'careerfy-frame'),
                'param_name' => 'soc_link',
            ),
        ),
    );
    $attributes = array(
        "name" => esc_html__("Contact Info", "careerfy-frame"),
        "base" => "careerfy_contact_info",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "class" => "",
        "params" => $params
    );

    if (function_exists('vc_map')) {
        vc_map($attributes);
    }
}

/**
 * Adding About information shortcode
 * @return markup
 */
function careerfy_vc_about_information() {

    $params = array();
    $params[] = array(
        'type' => 'textfield',
        'heading' => esc_html__("Title", "careerfy-frame"),
        'param_name' => 'abt_info_title',
        'value' => '',
        'description' => '',
    );
    $params[] = array(
        'type' => 'textfield',
        'heading' => esc_html__("Sub title", "careerfy-frame"),
        'param_name' => 'abt_sub_title',
        'value' => '',
        'description' => '',
    );
    $params[] = array(
        'type' => 'textarea',
        'heading' => esc_html__("User Description", "careerfy-frame"),
        'param_name' => 'abt_desc',
        'value' => '',
        'description' => '',
    );
    $params[] = array(
        'type' => 'textfield',
        'heading' => esc_html__("User Name", "careerfy-frame"),
        'param_name' => 'abt_name',
        'value' => '',
        'description' => '',
    );
    $params[] = array(
        'type' => 'textfield',
        'heading' => esc_html__("User Experience", "careerfy-frame"),
        'param_name' => 'abt_experi',
        'value' => '',
        'description' => '',
    );
    $params[] = array(
        'type' => 'param_group',
        'value' => '',
        'heading' => esc_html__("Social Links", "careerfy-frame"),
        'param_name' => 'abt_social_links',
        'params' => array(
            array(
                'type' => 'iconpicker',
                'value' => '',
                'heading' => __('Social Icon', 'careerfy-frame'),
                'param_name' => 'abt_soc_icon',
            ),
            array(
                'type' => 'textfield',
                'value' => '',
                'heading' => __('Social Link', 'careerfy-frame'),
                'param_name' => 'abt_soc_link',
            ),
        ),
    );
    $attributes = array(
        "name" => esc_html__("About Info", "careerfy-frame"),
        "base" => "careerfy_about_info",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "class" => "",
        "params" => $params
    );

    if (function_exists('vc_map')) {
        vc_map($attributes);
    }
}

/**
 * Adding Image Banner shortcode
 * @return markup
 */
function careerfy_vc_image_banner() {

    $attributes = array(
        "name" => esc_html__("Image Banner", "careerfy-frame"),
        "base" => "careerfy_image_banner",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "class" => "",
        "params" => array(
            array(
                'type' => 'careerfy_browse_img',
                'heading' => esc_html__("Background Image", "careerfy-frame"),
                'param_name' => 'b_bgimg',
                'value' => '',
                'description' => '',
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Title", "careerfy-frame"),
                'param_name' => 'b_title',
                'value' => '',
                'description' => '',
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("SubTitle", "careerfy-frame"),
                'param_name' => 'b_subtitle',
                'value' => '',
                'description' => '',
            ),
            array(
                'type' => 'textarea',
                'heading' => esc_html__("Description", "careerfy-frame"),
                'param_name' => 'b_desc',
                'value' => '',
                'description' => '',
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button 1 Text", "careerfy-frame"),
                'param_name' => 'btn1_txt',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button 1 URL", "careerfy-frame"),
                'param_name' => 'btn1_url',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button 2 Text", "careerfy-frame"),
                'param_name' => 'btn2_txt',
                'value' => '',
                'description' => '',
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button 2 URL", "careerfy-frame"),
                'param_name' => 'btn2_url',
                'value' => '',
                'description' => '',
            ),
        )
    );

    if (function_exists('vc_map')) {
        vc_map($attributes);
    }
}

/**
 * Adding section heading shortcode
 * @return markup
 */
function careerfy_vc_find_question() {

    $attributes = array(
        "name" => esc_html__("Find Question", "careerfy-frame"),
        "base" => "careerfy_find_question",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "class" => "",
        "params" => array(
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Serach Box", "careerfy-frame"),
                'param_name' => 'search_box',
                'value' => array(
                    esc_html__("Show", "careerfy-frame") => 'show',
                    esc_html__("Hide", "careerfy-frame") => 'hide',
                ),
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Title", "careerfy-frame"),
                'param_name' => 'search_title',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textarea',
                'heading' => esc_html__("Description", "careerfy-frame"),
                'param_name' => 'search_desc',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button Text", "careerfy-frame"),
                'param_name' => 'btn_txt',
                'value' => '',
                'description' => ''
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Button URL", "careerfy-frame"),
                'param_name' => 'btn_url',
                'value' => '',
                'description' => ''
            ),
        )
    );

    if (function_exists('vc_map')) {
        vc_map($attributes);
    }
}

/**
 * Counters shortcode
 * @return markup
 */
function careerfy_vc_counters_shortcode() {

    $attributes = array(
        "name" => esc_html__("Counters", "careerfy-frame"),
        "base" => "careerfy_counters",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "as_parent" => array('only' => 'careerfy_counters_item'),
        "content_element" => true,
        "show_settings_on_create" => false,
        "is_container" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Style", "careerfy-frame"),
                'param_name' => 'view',
                'value' => array(
                    esc_html__("Style 1", "careerfy-frame") => 'view-1',
                    esc_html__("Style 2", "careerfy-frame") => 'view-2',
                    esc_html__("Style 3", "careerfy-frame") => 'view-3',
                ),
                'description' => ''
            ),
            array(
                "type" => "colorpicker",
                "heading" => __("Icon Color", "careerfy-frame"),
                "param_name" => "counter_icon_color",
                'value' => '',
                "description" => '',
            ),
            array(
                "type" => "colorpicker",
                "heading" => __("Number Color", "careerfy-frame"),
                "param_name" => "counter_number_color",
                'value' => '',
                "description" => '',
            ),
            array(
                "type" => "colorpicker",
                "heading" => __("Title Color", "careerfy-frame"),
                "param_name" => "counter_title_color",
                'value' => '',
                "description" => '',
            ),
        ),
        "js_view" => 'VcColumnView'
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }

    $attributes = array(
        "name" => esc_html__("Counter Item", "careerfy-frame"),
        "base" => "careerfy_counters_item",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "content_element" => true,
        "as_child" => array('only' => 'careerfy_counters'),
        "show_settings_on_create" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                "type" => "iconpicker",
                "heading" => __("Icon", "careerfy-frame"),
                "param_name" => "count_icon",
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __("Number", "careerfy-frame"),
                "param_name" => "count_number",
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __("Title", "careerfy-frame"),
                "param_name" => "count_title",
                'value' => '',
                "description" => ''
            ),
        ),
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }

    if (class_exists('WPBakeryShortCodesContainer')) {

        class WPBakeryShortCode_Careerfy_Counters extends WPBakeryShortCodesContainer {
            
        }

    }

    if (class_exists('WPBakeryShortCode')) {

        class WPBakeryShortCode_Careerfy_Counters_Item extends WPBakeryShortCode {
            
        }

    }
}

/**
 * Services shortcode
 * @return markup
 */
function careerfy_vc_services_shortcode() {

    $attributes = array(
        "name" => esc_html__("Services", "careerfy-frame"),
        "base" => "careerfy_services",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "as_parent" => array('only' => 'careerfy_services_item'),
        "content_element" => true,
        "show_settings_on_create" => false,
        "is_container" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Style", "careerfy-frame"),
                'param_name' => 'view',
                'value' => array(
                    esc_html__("Style 1", "careerfy-frame") => 'view-1',
                    esc_html__("Style 2", "careerfy-frame") => 'view-2',
                    esc_html__("Style 3", "careerfy-frame") => 'view-3',
                    esc_html__("Style 4", "careerfy-frame") => 'view-4',
                    esc_html__("Style 5", "careerfy-frame") => 'view-5',
                ),
                'description' => ''
            ),
            array(
                "type" => "colorpicker",
                "heading" => __("Title Color", "careerfy-frame"),
                "param_name" => "service_title_color",
                'value' => '',
                "description" => '',
            ),
            array(
                "type" => "colorpicker",
                "heading" => __("Description Color", "careerfy-frame"),
                "param_name" => "service_text_color",
                'value' => '',
                "description" => '',
            ),
            array(
                "type" => "colorpicker",
                "heading" => __("Icon Color", "careerfy-frame"),
                "param_name" => "service_icon_color",
                'value' => '',
                "description" => '',
            ),
        ),
        "js_view" => 'VcColumnView'
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }

    $attributes = array(
        "name" => esc_html__("Service Item", "careerfy-frame"),
        "base" => "careerfy_services_item",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "content_element" => true,
        "as_child" => array('only' => 'careerfy_services'),
        "show_settings_on_create" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                'type' => 'careerfy_browse_img',
                'heading' => esc_html__("Image", "careerfy-frame"),
                'param_name' => 'service_img',
                'value' => '',
                'description' => ''
            ),
            array(
                "type" => "iconpicker",
                "heading" => __("Icon", "careerfy-frame"),
                "param_name" => "service_icon",
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "colorpicker",
                "heading" => __("Background Color", "careerfy-frame"),
                "param_name" => "service_bg",
                'value' => '',
                "description" => __("This option will apply on 'Service style 4' only.", "careerfy-frame"),
            ),
            array(
                "type" => "textfield",
                "heading" => __("Title", "careerfy-frame"),
                "param_name" => 'service_title',
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __("Link", "careerfy-frame"),
                "param_name" => 'service_link',
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textarea",
                "heading" => __("Description", "careerfy-frame"),
                "param_name" => "service_desc",
                'value' => '',
                "description" => ''
            ),
        ),
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }

    if (class_exists('WPBakeryShortCodesContainer')) {

        class WPBakeryShortCode_Careerfy_Services extends WPBakeryShortCodesContainer {
            
        }

    }

    if (class_exists('WPBakeryShortCode')) {

        class WPBakeryShortCode_Careerfy_Services_Item extends WPBakeryShortCode {
            
        }

    }
}

/**
 * Image Services shortcode
 * @return markup
 */
function careerfy_vc_image_services_shortcode() {

    $attributes = array(
        "name" => esc_html__("Image Services", "careerfy-frame"),
        "base" => "careerfy_image_services",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "as_parent" => array('only' => 'careerfy_image_services_item'),
        "content_element" => true,
        "show_settings_on_create" => false,
        "is_container" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Style", "careerfy-frame"),
                'param_name' => 'service_view',
                'value' => array(
                    esc_html__("Style 1", "careerfy-frame") => 'view1',
                    esc_html__("Style 2", "careerfy-frame") => 'view2',
                ),
                'description' => ''
            ),
        ),
        "js_view" => 'VcColumnView'
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }

    $attributes = array(
        "name" => esc_html__("Service Item", "careerfy-frame"),
        "base" => "careerfy_image_services_item",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "content_element" => true,
        "as_child" => array('only' => 'careerfy_image_services'),
        "show_settings_on_create" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                "type" => "careerfy_browse_img",
                "heading" => __("Image", "careerfy-frame"),
                "param_name" => "service_img",
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __("Title", "careerfy-frame"),
                "param_name" => 'service_title',
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textarea",
                "heading" => __("Description", "careerfy-frame"),
                "param_name" => "service_desc",
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __("Link", "careerfy-frame"),
                "param_name" => 'service_link',
                'value' => '',
                "description" => ''
            ),
        ),
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }

    if (class_exists('WPBakeryShortCodesContainer')) {

        class WPBakeryShortCode_Careerfy_Image_Services extends WPBakeryShortCodesContainer {
            
        }

    }

    if (class_exists('WPBakeryShortCode')) {

        class WPBakeryShortCode_Careerfy_Image_Services_Item extends WPBakeryShortCode {
            
        }

    }
}

/**
 * Our Partners shortcode
 * @return markup
 */
function careerfy_vc_our_partners_shortcode() {

    $attributes = array(
        "name" => esc_html__("Our Partners", "careerfy-frame"),
        "base" => "careerfy_our_partners",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "as_parent" => array('only' => 'careerfy_our_partners_item'),
        "content_element" => true,
        "show_settings_on_create" => false,
        "is_container" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Style", "careerfy-frame"),
                'param_name' => 'partner_view',
                'value' => array(
                    esc_html__("Style 1", "careerfy-frame") => 'view1',
                    esc_html__("Style 2", "careerfy-frame") => 'view2',
                    esc_html__("Style 3", "careerfy-frame") => 'view3',
                    esc_html__("Style 4", "careerfy-frame") => 'view4',
                ),
                'description' => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __("Title", "careerfy-frame"),
                "param_name" => 'partner_title',
                'value' => '',
                "description" => ''
            ),
        ),
        "js_view" => 'VcColumnView'
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }

    $attributes = array(
        "name" => esc_html__("Partner Item", "careerfy-frame"),
        "base" => "careerfy_our_partners_item",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "content_element" => true,
        "as_child" => array('only' => 'careerfy_our_partners'),
        "show_settings_on_create" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                "type" => "careerfy_browse_img",
                "heading" => __("Image", "careerfy-frame"),
                "param_name" => "partner_img",
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __("URL", "careerfy-frame"),
                "param_name" => 'partner_url',
                'value' => '',
                "description" => ''
            ),
        ),
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }

    if (class_exists('WPBakeryShortCodesContainer')) {

        class WPBakeryShortCode_Careerfy_Our_Partners extends WPBakeryShortCodesContainer {
            
        }

    }

    if (class_exists('WPBakeryShortCode')) {

        class WPBakeryShortCode_Careerfy_Our_Partners_Item extends WPBakeryShortCode {
            
        }

    }
}

/**
 * Recent Questions shortcode
 * @return markup
 */
function careerfy_vc_recent_questions() {

    $attributes = array(
        "name" => esc_html__("Recent Questions", "careerfy-frame"),
        "base" => "careerfy_recent_questions",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "as_parent" => array('only' => 'careerfy_recent_questions_item'),
        "content_element" => true,
        "show_settings_on_create" => false,
        "is_container" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                "type" => "textfield",
                "heading" => __("Title", "careerfy-frame"),
                "param_name" => "ques_title",
                'value' => '',
                "description" => ''
            ),
        ),
        "js_view" => 'VcColumnView'
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }

    $attributes = array(
        "name" => esc_html__("Question Item", "careerfy-frame"),
        "base" => "careerfy_recent_questions_item",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "content_element" => true,
        "as_child" => array('only' => 'careerfy_recent_questions'),
        "show_settings_on_create" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                "type" => "textfield",
                "heading" => __("Question", "careerfy-frame"),
                "param_name" => 'q_question',
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __("Link", "careerfy-frame"),
                "param_name" => "q_url",
                'value' => '',
                "description" => ''
            ),
        ),
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }

    if (class_exists('WPBakeryShortCodesContainer')) {

        class WPBakeryShortCode_Careerfy_Recent_Questions extends WPBakeryShortCodesContainer {
            
        }

    }

    if (class_exists('WPBakeryShortCode')) {

        class WPBakeryShortCode_Careerfy_Recent_Questions_Item extends WPBakeryShortCode {
            
        }

    }
}

/**
 * FAQs shortcode
 * @return markup
 */
function careerfy_vc_faqs_shortcode() {

    $categories = get_terms(array(
        'taxonomy' => 'faq-category',
        'hide_empty' => false,
    ));

    $cate_array = array(esc_html__("Select Category", "careerfy-frame") => '');
    if (is_array($categories) && sizeof($categories) > 0) {
        foreach ($categories as $category) {
            $cate_array[$category->name] = $category->slug;
        }
    }

    $attributes = array(
        "name" => esc_html__("FAQs", "careerfy-frame"),
        "base" => "careerfy_faqs",
        "class" => "",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "params" => array(
            // add params same as with any other content element
            array(
                "type" => "textfield",
                "heading" => __("Title", "careerfy-frame"),
                "param_name" => "ques_title",
                'value' => '',
                "description" => ''
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Open First Question", "careerfy-frame"),
                'param_name' => 'op_first_q',
                'value' => array(
                    esc_html__("Yes", "careerfy-frame") => 'yes',
                    esc_html__("No", "careerfy-frame") => 'no',
                ),
                'description' => ''
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Category", "careerfy-frame"),
                'param_name' => 'faq_cat',
                'value' => $cate_array,
                'description' => esc_html__("Select Category.", "careerfy-frame")
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("Excerpt Length", "careerfy-frame"),
                'param_name' => 'faq_excerpt',
                'value' => '20',
                'description' => esc_html__("Set number of words you want show for faq answer.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Order", "careerfy-frame"),
                'param_name' => 'faq_order',
                'value' => array(
                    esc_html__("Descending", "careerfy-frame") => 'DESC',
                    esc_html__("Ascending", "careerfy-frame") => 'ASC',
                ),
                'description' => esc_html__("Choose faq list items order.", "careerfy-frame")
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Orderby", "careerfy-frame"),
                'param_name' => 'faq_orderby',
                'value' => array(
                    esc_html__("Date", "careerfy-frame") => 'date',
                    esc_html__("Title", "careerfy-frame") => 'title',
                ),
                'description' => esc_html__("Choose faq list items orderby.", "careerfy-frame")
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__("No of Questions", "careerfy-frame"),
                'param_name' => 'num_of_faqs',
                'value' => '10',
                'description' => ''
            ),
        ),
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }
}

/**
 * Our Team shortcode
 * @return markup
 */
function careerfy_vc_our_team_shortcode() {

    $attributes = array(
        "name" => esc_html__("Our Team", "careerfy-frame"),
        "base" => "careerfy_our_team",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "as_parent" => array('only' => 'careerfy_our_team_item'),
        "content_element" => true,
        "show_settings_on_create" => false,
        "is_container" => true,
        "params" => array(
        // add params same as with any other content element
        ),
        "js_view" => 'VcColumnView'
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }

    $attributes = array(
        "name" => esc_html__("Team Item", "careerfy-frame"),
        "base" => "careerfy_our_team_item",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "content_element" => true,
        "as_child" => array('only' => 'careerfy_our_team'),
        "show_settings_on_create" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                "type" => "careerfy_browse_img",
                "heading" => __("Image", "careerfy-frame"),
                "param_name" => "team_img",
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __("Title", "careerfy-frame"),
                "param_name" => 'team_title',
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __("Position", "careerfy-frame"),
                "param_name" => "team_pos",
                'value' => '',
                "description" => ''
            ),
        ),
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }

    if (class_exists('WPBakeryShortCodesContainer')) {

        class WPBakeryShortCode_Careerfy_Our_Team extends WPBakeryShortCodesContainer {
            
        }

    }

    if (class_exists('WPBakeryShortCode')) {

        class WPBakeryShortCode_Careerfy_Our_Team_Item extends WPBakeryShortCode {
            
        }

    }
}

/**
 * Help Links shortcode
 * @return markup
 */
function careerfy_vc_help_links_shortcode() {

    $attributes = array(
        "name" => esc_html__("Help Links", "careerfy-frame"),
        "base" => "careerfy_help_links",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "as_parent" => array('only' => 'careerfy_help_links_item'),
        "content_element" => true,
        "show_settings_on_create" => false,
        "is_container" => true,
        "params" => array(
        // add params same as with any other content element
        ),
        "js_view" => 'VcColumnView'
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }

    $attributes = array(
        "name" => esc_html__("Help Item", "careerfy-frame"),
        "base" => "careerfy_help_links_item",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "content_element" => true,
        "as_child" => array('only' => 'careerfy_help_links'),
        "show_settings_on_create" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                "type" => "iconpicker",
                "heading" => __("Icon", "careerfy-frame"),
                "param_name" => "help_icon",
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __("Title", "careerfy-frame"),
                "param_name" => 'help_title',
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __("Button Text", "careerfy-frame"),
                "param_name" => "btn_txt",
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __("Button URL", "careerfy-frame"),
                "param_name" => "btn_url",
                'value' => '',
                "description" => ''
            ),
        ),
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }

    if (class_exists('WPBakeryShortCodesContainer')) {

        class WPBakeryShortCode_Careerfy_Help_Links extends WPBakeryShortCodesContainer {
            
        }

    }

    if (class_exists('WPBakeryShortCode')) {

        class WPBakeryShortCode_Careerfy_Help_Links_Item extends WPBakeryShortCode {
            
        }

    }
}

/**
 * Testimonials with image shortcode
 * @return markup
 */
function careerfy_vc_testimonials_with_image() {

    $attributes = array(
        "name" => esc_html__("Testimonials", "careerfy-frame"),
        "base" => "careerfy_testimonials",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "as_parent" => array('only' => 'careerfy_testimonial_item'),
        "content_element" => true,
        "show_settings_on_create" => false,
        "is_container" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                'type' => 'dropdown',
                'heading' => esc_html__("Style", "careerfy-frame"),
                'param_name' => 'testi_view',
                'value' => array(
                    esc_html__("Style 1", "careerfy-frame") => 'view1',
                    esc_html__("Style 2", "careerfy-frame") => 'view2',
                    esc_html__("Style 3", "careerfy-frame") => 'view3',
                    esc_html__("Style 4", "careerfy-frame") => 'view4',
                ),
                'description' => ''
            ),
            array(
                "type" => "careerfy_browse_img",
                "heading" => __("Image", "careerfy-frame"),
                "param_name" => "img",
                'value' => '',
                "description" => ''
            ),
        ),
        "js_view" => 'VcColumnView'
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }

    $attributes = array(
        "name" => esc_html__("Testimonial Item", "careerfy-frame"),
        "base" => "careerfy_testimonial_item",
        "category" => esc_html__("Careerfy Theme", "careerfy-frame"),
        "content_element" => true,
        "as_child" => array('only' => 'careerfy_testimonials'),
        "show_settings_on_create" => true,
        "params" => array(
            // add params same as with any other content element
            array(
                "type" => "careerfy_browse_img",
                "heading" => __("Image", "careerfy-frame"),
                "param_name" => "img",
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textarea",
                "heading" => __("Text", "careerfy-frame"),
                "param_name" => "desc",
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __("Title", "careerfy-frame"),
                "param_name" => "title",
                'value' => '',
                "description" => ''
            ),
            array(
                "type" => "textfield",
                "heading" => __("Position", "careerfy-frame"),
                "param_name" => "position",
                'value' => '',
                "description" => ''
            ),
        ),
    );
    if (function_exists('vc_map')) {
        vc_map($attributes);
    }

    if (class_exists('WPBakeryShortCodesContainer')) {

        class WPBakeryShortCode_Careerfy_Testimonials extends WPBakeryShortCodesContainer {
            
        }

    }

    if (class_exists('WPBakeryShortCode')) {

        class WPBakeryShortCode_Careerfy_Testimonial_Item extends WPBakeryShortCode {
            
        }

    }
}
