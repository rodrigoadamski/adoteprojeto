<?php
/**
 * Section Heading Shortcode
 * @return html
 */
add_shortcode('careerfy_section_heading', 'careerfy_section_heading_shortcode');

function careerfy_section_heading_shortcode($atts) {
    extract(shortcode_atts(array(
        'view' => '',
		'h_fancy_title' => '',
        'h_title' => '',
        'hc_title' => '',
        'hc_title_clr' => '',
        'hc_icon' => '',
        'h_desc' => '',
        'hc_dcolor' => '',
                    ), $atts));

    ob_start();

    $title_colr_style = '';
    if ($hc_title_clr != '') {
        $title_colr_style = ' style="color: ' . $hc_title_clr . ';"';
    }
    
    $desc_colr_style = '';
    if ($hc_dcolor != '') {
        $desc_colr_style = ' style="color: ' . $hc_dcolor . ';"';
    }

    $hdng_con = 'section';
    $hdng_class = 'careerfy-fancy-title';
    if ($view == 'view2') {
        $hdng_class = 'careerfy-fancy-title careerfy-fancy-title-two';
        $hdng_con = 'div';
    } else if ($view == 'view3') {
        $hdng_class = 'careerfy-fancy-title careerfy-fancy-title-three';
        $hdng_con = 'div';
    } else if ($view == 'view4') {
        $hdng_class = 'careerfy-fancy-title careerfy-fancy-title-four';
        $hdng_con = 'div';
    } else if ($view == 'view5') {
        $hdng_class = 'careerfy-fancy-title careerfy-fancy-title-six';
        $hdng_con = 'div';
    }
    ?>
    <<?php echo ($hdng_con) ?> class="<?php echo ($hdng_class) ?>">
    <?php echo ($hc_icon != '' && $view == 'view3' ? '<i class="' . $hc_icon . '"></i>' : '') ?>
    <?php echo ($h_fancy_title != '' && $view == 'view5' ? '<span>' . $h_fancy_title . '</span>' : '') ?>
    <h2><?php echo ($h_title) ?> <?php echo ($hc_title != '' ? '<span' . $title_colr_style . '>' . $hc_title . '</span>' : '') ?></h2>
    <?php
    if ($h_desc != '') {
        ?>
        <p<?php echo ($desc_colr_style) ?>><?php echo ($h_desc) ?></p>
        <?php
    }
    echo ($view == 'view4' ? '<span> <i class="fa fa-circle"' . $desc_colr_style . '></i> <i class="fa fa-circle circle-two-size"' . $desc_colr_style . '></i> <i class="fa fa-circle circle-three-size"' . $desc_colr_style . '></i> </span>' : '');
    ?>
    </<?php echo ($hdng_con) ?>>

    <?php
    $html = ob_get_clean();

    return $html;
}
