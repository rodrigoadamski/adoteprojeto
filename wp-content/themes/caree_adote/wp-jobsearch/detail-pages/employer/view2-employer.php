<?php
global $post, $jobsearch_plugin_options;
$employer_id = $post->ID;

$captcha_switch = isset($jobsearch_plugin_options['captcha_switch']) ? $jobsearch_plugin_options['captcha_switch'] : '';
$jobsearch_sitekey = isset($jobsearch_plugin_options['captcha_sitekey']) ? $jobsearch_plugin_options['captcha_sitekey'] : '';

$all_location_allow = isset($jobsearch_plugin_options['all_location_allow']) ? $jobsearch_plugin_options['all_location_allow'] : '';
$job_types_switch = isset($jobsearch_plugin_options['job_types_switch']) ? $jobsearch_plugin_options['job_types_switch'] : '';

$plugin_default_view = isset($jobsearch_plugin_options['jobsearch-default-page-view']) ? $jobsearch_plugin_options['jobsearch-default-page-view'] : 'full';
$plugin_default_view_with_str = '';
if ($plugin_default_view == 'boxed') {

    $plugin_default_view_with_str = isset($jobsearch_plugin_options['jobsearch-boxed-view-width']) && $jobsearch_plugin_options['jobsearch-boxed-view-width'] != '' ? $jobsearch_plugin_options['jobsearch-boxed-view-width'] : '1140px';
    if ($plugin_default_view_with_str != '') {
        $plugin_default_view_with_str = ' style="width:' . $plugin_default_view_with_str . '"';
    }
}

$employer_views_count = get_post_meta($employer_id, "jobsearch_employer_views_count", true);

//
$user_facebook_url = get_post_meta($employer_id, 'jobsearch_field_user_facebook_url', true);
$user_twitter_url = get_post_meta($employer_id, 'jobsearch_field_user_twitter_url', true);
$user_google_plus_url = get_post_meta($employer_id, 'jobsearch_field_user_google_plus_url', true);
$user_youtube_url = get_post_meta($employer_id, 'jobsearch_field_user_youtube_url', true);
$user_dribbble_url = get_post_meta($employer_id, 'jobsearch_field_user_dribbble_url', true);
$user_linkedin_url = get_post_meta($employer_id, 'jobsearch_field_user_linkedin_url', true);

$sectors_enable_switch = isset($jobsearch_plugin_options['sectors_onoff_switch']) ? $jobsearch_plugin_options['sectors_onoff_switch'] : '';

$employer_obj = get_post($employer_id);
$employer_content = $employer_obj->post_content;
$employer_content = apply_filters('the_content', $employer_content);

$employer_join_date = isset($employer_obj->post_date) ? $employer_obj->post_date : '';

$employer_address = get_post_meta($employer_id, 'jobsearch_field_location_address', true);

if ($employer_address == '') {
    $employer_address = jobsearch_job_item_address($employer_id);
}

$employer_phone = get_post_meta($employer_id, 'jobsearch_field_user_phone', true);

$user_id = jobsearch_get_employer_user_id($employer_id);
$user_obj = get_user_by('ID', $user_id);
$user_displayname = isset($user_obj->display_name) ? $user_obj->display_name : '';
$user_displayname = apply_filters('jobsearch_user_display_name', $user_displayname, $user_obj);

$user_def_avatar_url = get_avatar_url($user_id, array('size' => 140));

$user_avatar_id = get_post_thumbnail_id($employer_id);
if ($user_avatar_id > 0) {
    $user_thumbnail_image = wp_get_attachment_image_src($user_avatar_id, 'thumbnail');
    $user_def_avatar_url = isset($user_thumbnail_image[0]) && esc_url($user_thumbnail_image[0]) != '' ? $user_thumbnail_image[0] : '';
}
$user_def_avatar_url = $user_def_avatar_url == '' ? jobsearch_employer_image_placeholder() : $user_def_avatar_url;
wp_enqueue_script('isotope-min');
wp_enqueue_style('careerfy-emp-detail-two');
?>

<div class="careerfy-main-content">

    <!-- Main Section -->
    <div class="careerfy-main-section">
        <div class="container">
            <div class="row">
                <div class="careerfy-column-12">
                    <div class="careerfy-employer-detail2-toparea">
                        <figure><a><img src="<?php echo ($user_def_avatar_url) ?>" alt=""></a>
                            <figcaption>
                                <h2><?php echo ($user_displayname) ?></h2>
                                <?php
                                $post_avg_review_args = array(
                                    'post_id' => $employer_id,
                                    'prefix' => 'careerfy',
                                    'total_revs' => 'yes',
                                );
                                do_action('jobsearch_post_avg_rating', $post_avg_review_args);
                                ?>
                            </figcaption>
                        </figure>
                        <div class="careerfy-right">
                            <?php
                            $reviews_switch = isset($jobsearch_plugin_options['reviews_switch']) ? $jobsearch_plugin_options['reviews_switch'] : '';
                            if ($reviews_switch == 'on') {
                                if (is_user_logged_in()) {
                                    wp_enqueue_script('jobsearch-barrating');
                                    wp_enqueue_script('jobsearch-add-review');
                                    ?>
                                    <a href="javascript:void(0);" data-target="add_review_form_sec" class="careerfy-employer-detail2-toparea-btn jobsearch-go-to-review-form" data-post_id="<?php echo ($employer_id) ?>"><i class="careerfy-icon careerfy-add"></i> <?php esc_html_e('Add a review', 'wp-jobsearch') ?></a>
                                    <?php
                                } else {
                                    ?>
                                    <a href="javascript:void(0);" class="careerfy-employer-detail2-toparea-btn jobsearch-open-signin-tab"><i class="careerfy-icon careerfy-add"></i> <?php esc_html_e('Add a review', 'wp-jobsearch') ?></a>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="careerfy-employer-detail2-tablink">
                        <ul id="employer-detail2-tabs">
                            <li class="active"><a href="javascript:void(0);" class="div-to-scroll" data-target="careerfy-overview-sec"><?php esc_html_e('OverView', 'wp-jobsearch') ?></a></li>
                            <?php
                            if ($employer_content != '') {
                                ?>
                                <li><a href="javascript:void(0);" class="div-to-scroll" data-target="careerfy-compdesc-sec"><?php esc_html_e('Company Description', 'wp-jobsearch') ?></a></li>
                                <?php
                            }
                            $exfield_list = get_post_meta($employer_id, 'jobsearch_field_team_title', true);
                            if (is_array($exfield_list) && sizeof($exfield_list) > 0) {
                                ?>
                                <li><a href="javascript:void(0);" class="div-to-scroll" data-target="careerfy-teammemb-sec"><?php esc_html_e('Team Members', 'wp-jobsearch') ?></a></li>
                                <?php
                            }
                            $company_gal_imgs = get_post_meta($employer_id, 'jobsearch_field_company_gallery_imgs', true);
                            if (!empty($company_gal_imgs)) {
                                ?>
                                <li><a href="javascript:void(0);" class="div-to-scroll" data-target="careerfy-oficphots-sec"><?php esc_html_e('Office Photos', 'wp-jobsearch') ?></a></li>
                                <?php
                            }
                            $reviews_switch = isset($jobsearch_plugin_options['reviews_switch']) ? $jobsearch_plugin_options['reviews_switch'] : '';
                            if ($reviews_switch == 'on') {
                                $comen_args = array(
                                    'post_id' => $employer_id,
                                    'status' => 'approve',
                                );
                                $all_comments = get_comments($comen_args);
                                if (!empty($all_comments)) {
                                    ?>
                                    <li><a href="javascript:void(0);" class="div-to-scroll" data-target="careerfy-reviws-sec"><?php esc_html_e('Reviews', 'wp-jobsearch') ?></a></li>
                                    <?php
                                }
                            }
                            ?>
                        </ul>
                    </div>
                </div>

                <!-- Job Detail Content -->
                <div class="careerfy-column-8 careerfy-typo-wrap">
                    <div class="employer-content-wrapper">
                        <?php
                        $custom_all_fields = get_option('jobsearch_custom_field_employer');
                        ?>
                        <div class="careerfy-jobdetail-content careerfy-employerdetail-twocontent">
                            <?php
                            if (!empty($custom_all_fields)) {
                                $sector_str = jobsearch_employer_get_all_sectors($employer_id, '', '', '', '<small>', '</small>');
                                $sector_str = apply_filters('jobsearch_gew_wout_anchr_sector_str_html', $sector_str, $employer_id, '<small>', '</small>');
                                ?>
                                <div id="careerfy-overview-sec" class="careerfy-content-title"><h2><?php esc_html_e('Overview', 'wp-jobsearch') ?></h2></div>
                                <div class="careerfy-jobdetail-services">
                                    <ul class="careerfy-row">
                                        <?php
                                        if ($sectors_enable_switch == 'on') {
                                            ?>
                                            <li class="careerfy-column-4">
                                                <i class="careerfy-icon careerfy-folder"></i>
                                                <div class="careerfy-services-text"><?php esc_html_e('Sectors', 'wp-jobsearch') ?> <?php echo wp_kses($sector_str, array('small' => array())) ?></div>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                        <li class="careerfy-column-4">
                                            <i class="careerfy-icon careerfy-briefcase"></i>
                                            <div class="careerfy-services-text"><?php esc_html_e('Posted Jobs', 'wp-jobsearch') ?> <small><?php echo jobsearch_employer_total_jobs_posted($employer_id) ?></small></div>
                                        </li>
                                        <li class="careerfy-column-4">
                                            <i class="careerfy-icon careerfy-view"></i>
                                            <div class="careerfy-services-text"><?php esc_html_e('Viewed', 'wp-jobsearch') ?> <small><?php echo ($employer_views_count) ?></small></div>
                                        </li>
                                        <?php
                                        if (!empty($custom_all_fields)) {
                                            $cus_fields = array('content' => '');
                                            $cus_fields = apply_filters('jobsearch_custom_fields_list', 'employer', $employer_id, $cus_fields, '<li class="careerfy-column-4">', '</li>', '', true, true, true, 'careerfy');
                                            if (isset($cus_fields['content']) && $cus_fields['content'] != '') {
                                                echo ($cus_fields['content']);
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <?php
                                if ($employer_content != '') {
                                    ?>
                                    <div id="careerfy-compdesc-sec" class="careerfy-content-title"><h2><?php esc_html_e('Company Description', 'wp-jobsearch') ?></h2></div>
                                    <div class="jobsearch-description">
                                        <?php echo ($employer_content) ?>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                        }
                        $exfield_list = get_post_meta($employer_id, 'jobsearch_field_team_title', true);
                        $exfield_list_val = get_post_meta($employer_id, 'jobsearch_field_team_description', true);
                        $team_designationfield_list = get_post_meta($employer_id, 'jobsearch_field_team_designation', true);
                        $team_experiencefield_list = get_post_meta($employer_id, 'jobsearch_field_team_experience', true);
                        $team_imagefield_list = get_post_meta($employer_id, 'jobsearch_field_team_image', true);
                        $team_facebookfield_list = get_post_meta($employer_id, 'jobsearch_field_team_facebook', true);
                        $team_googlefield_list = get_post_meta($employer_id, 'jobsearch_field_team_google', true);
                        $team_twitterfield_list = get_post_meta($employer_id, 'jobsearch_field_team_twitter', true);
                        $team_linkedinfield_list = get_post_meta($employer_id, 'jobsearch_field_team_linkedin', true);

                        if (is_array($exfield_list) && sizeof($exfield_list) > 0) {
                            $total_team = sizeof($exfield_list);

                            $rand_num_ul = rand(1000000, 99999999);
                            ?>
                            <div id="careerfy-teammemb-sec" class="careerfy-employer-wrap-section careerfy-employerdetail-twocontent bottom-none">
                                <div class="careerfy-content-title careerfy-addmore-space"><h2><?php printf(esc_html__('Team Members (%s)', 'wp-jobsearch'), $total_team); ?></h2></div>
                                <div class="jobsearch-candidate jobsearch-candidate-grid">
                                    <ul id="members-holder-<?php echo absint($rand_num_ul) ?>" class="careerfy-row">
                                        <?php
                                        $exfield_counter = 0;
                                        foreach ($exfield_list as $exfield) {
                                            $rand_num = rand(1000000, 99999999);

                                            $exfield_val = isset($exfield_list_val[$exfield_counter]) ? $exfield_list_val[$exfield_counter] : '';
                                            $team_designationfield_val = isset($team_designationfield_list[$exfield_counter]) ? $team_designationfield_list[$exfield_counter] : '';
                                            $team_experiencefield_val = isset($team_experiencefield_list[$exfield_counter]) ? $team_experiencefield_list[$exfield_counter] : '';
                                            $team_imagefield_val = isset($team_imagefield_list[$exfield_counter]) ? $team_imagefield_list[$exfield_counter] : '';
                                            $team_facebookfield_val = isset($team_facebookfield_list[$exfield_counter]) ? $team_facebookfield_list[$exfield_counter] : '';
                                            $team_googlefield_val = isset($team_googlefield_list[$exfield_counter]) ? $team_googlefield_list[$exfield_counter] : '';
                                            $team_twitterfield_val = isset($team_twitterfield_list[$exfield_counter]) ? $team_twitterfield_list[$exfield_counter] : '';
                                            $team_linkedinfield_val = isset($team_linkedinfield_list[$exfield_counter]) ? $team_linkedinfield_list[$exfield_counter] : '';
                                            ?>
                                            <li class="careerfy-column-4">
                                                <script>
                                                    jQuery(document).ready(function () {
                                                        jQuery('a[id^="fancybox_notes"]').fancybox({
                                                            'titlePosition': 'inside',
                                                            'transitionIn': 'elastic',
                                                            'transitionOut': 'elastic',
                                                            'width': 400,
                                                            'height': 250,
                                                            'padding': 40,
                                                            'autoSize': false
                                                        });
                                                    });
                                                </script>
                                                <figure>
                                                    <a id="fancybox_notes<?php echo ($rand_num) ?>" href="#notes<?php echo ($rand_num) ?>" class="jobsearch-candidate-grid-thumb"><img src="<?php echo ($team_imagefield_val) ?>" alt=""> <span class="jobsearch-candidate-grid-status"></span></a>
                                                    <figcaption>
                                                        <h2><a id="fancybox_notes_txt<?php echo ($rand_num) ?>" href="#notes<?php echo ($rand_num) ?>"><?php echo ($exfield) ?></a></h2>
                                                        <p><?php echo ($team_designationfield_val) ?></p>
                                                        <?php
                                                        if ($team_experiencefield_val != '') {
                                                            echo '<span>' . sprintf(esc_html__('Experience: %s', 'wp-jobsearch'), $team_experiencefield_val) . '</span>';
                                                        }
                                                        ?>
                                                    </figcaption>
                                                </figure>

                                                <div id="notes<?php echo ($rand_num) ?>" style="display: none;"><?php echo ($exfield_val) ?></div>
                                                <?php
                                                if ($team_facebookfield_val != '' || $team_googlefield_val != '' || $team_twitterfield_val != '' || $team_linkedinfield_val != '') {
                                                    ?>
                                                    <ul class="jobsearch-social-icons">
                                                        <?php
                                                        if ($team_facebookfield_val != '') {
                                                            ?>
                                                            <li><a href="<?php echo ($team_facebookfield_val) ?>" data-original-title="facebook" class="jobsearch-icon jobsearch-facebook-logo"></a></li>
                                                            <?php
                                                        }
                                                        if ($team_googlefield_val != '') {
                                                            ?>
                                                            <li><a href="<?php echo ($team_googlefield_val) ?>" data-original-title="google-plus" class="jobsearch-icon jobsearch-google-plus-logo-button"></a></li>
                                                            <?php
                                                        }
                                                        if ($team_twitterfield_val != '') {
                                                            ?>
                                                            <li><a href="<?php echo ($team_twitterfield_val) ?>" data-original-title="twitter" class="jobsearch-icon jobsearch-twitter-logo"></a></li>
                                                            <?php
                                                        }
                                                        if ($team_linkedinfield_val != '') {
                                                            ?>
                                                            <li><a href="<?php echo ($team_linkedinfield_val) ?>" data-original-title="linkedin" class="jobsearch-icon jobsearch-linkedin-button"></a></li>
                                                            <?php
                                                        }
                                                        ?>
                                                    </ul>
                                                    <?php
                                                }
                                                ?>
                                            </li>
                                            <?php
                                            $exfield_counter++;

                                            if ($exfield_counter >= 3) {
                                                break;
                                            }
                                        }
                                        ?>
                                    </ul>
                                </div>
                                <?php
                                $reults_per_page = 3;
                                $total_pages = 1;
                                if ($total_team > 0 && $reults_per_page > 0 && $total_team > $reults_per_page) {
                                    $total_pages = ceil($total_team / $reults_per_page);
                                    ?>
                                    <div class="jobsearch-load-more">
                                        <a class="load-more-team" href="javascript:void(0);" data-id="<?php echo ($employer_id) ?>" data-pref="careerfy" data-rand="<?php echo ($rand_num_ul) ?>" data-pages="<?php echo ($total_pages) ?>" data-page="1"><?php esc_html_e('Load More', 'wp-jobsearch') ?></a>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                            <?php
                        }
                        //
                        $company_gal_imgs = get_post_meta($employer_id, 'jobsearch_field_company_gallery_imgs', true);
                        $company_gal_videos = get_post_meta($employer_id, 'jobsearch_field_company_gallery_videos', true);
                        if (!empty($company_gal_imgs)) {
                            ?>
                            <div id="careerfy-oficphots-sec" class="careerfy-employer-wrap-section careerfy-employerdetail-twocontent">
                                <div class="jcareerfy-content-title careerfy-addmore-space"><h2><?php esc_html_e('Office Photos', 'wp-jobsearch') ?></h2></div>
                                <div class="careerfy-gallery careerfy-employer-gallery">
                                    <ul class="careerfy-row">
                                        <?php
                                        $profile_gal_counter = 1;
                                        $_gal_img_counter = 0;
                                        foreach ($company_gal_imgs as $company_gal_img) {
                                            if ($company_gal_img != '' && absint($company_gal_img) <= 0) {
                                                $company_gal_img = jobsearch_get_attachment_id_from_url($company_gal_img);
                                            }
                                            $gal_thumbnail_image = wp_get_attachment_image_src($company_gal_img, 'large');
                                            $gal_thumb_image_src = isset($gal_thumbnail_image[0]) && esc_url($gal_thumbnail_image[0]) != '' ? $gal_thumbnail_image[0] : '';

                                            $gal_video_url = isset($company_gal_videos[$_gal_img_counter]) && ($company_gal_videos[$_gal_img_counter]) != '' ? $company_gal_videos[$_gal_img_counter] : '';
                                            if ($gal_video_url != '') {

                                                if (strpos($gal_video_url, 'watch?v=') !== false) {
                                                    $gal_video_url = str_replace('watch?v=', 'embed/', $gal_video_url);
                                                }

                                                if (strpos($gal_video_url, '?') !== false) {
                                                    $gal_video_url .= '&autoplay=1';
                                                } else {
                                                    $gal_video_url .= '?autoplay=1';
                                                }
                                            }

                                            $gal_full_image = wp_get_attachment_image_src($company_gal_img, 'full');
                                            $gal_full_image_src = isset($gal_full_image[0]) && esc_url($gal_full_image[0]) != '' ? $gal_full_image[0] : '';
                                            ?>
                                            <li class="grid-item <?php echo ($profile_gal_counter > 2 ? 'careerfy-column-4' : 'careerfy-column-6') ?>"> 
                                                <a href="<?php echo ($gal_video_url != '' ? $gal_video_url : $gal_full_image_src) ?>" class="<?php echo ($gal_video_url != '' ? 'fancybox-video' : 'fancybox') ?>" <?php echo ($gal_video_url != '' ? 'data-fancybox-type="iframe"' : '') ?> data-fancybox-group="group">
                                                    <span class="grid-item-thumb"><small style="background-image: url('<?php echo ($gal_thumb_image_src) ?>');"></small></span>
                                                </a>
                                            </li>
                                            <?php
                                            $profile_gal_counter++;
                                            $_gal_img_counter++;
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                            <?php
                        }

                        $post_reviews_args = array(
                            'post_id' => $employer_id,
                            'prefix' => 'careerfy',
                            'div_id' => 'careerfy-reviws-sec',
                            'main_class' => 'careerfy-employer-wrap-section careerfy-margin-bottom careerfy-employerdetail-twocontent',
                            'list_label' => esc_html__('Company Reviews', 'wp-jobsearch'),
                        );
                        do_action('jobsearch_post_reviews_list', $post_reviews_args);

                        $review_form_args = array(
                            'post_id' => $employer_id,
                        );
                        do_action('jobsearch_add_review_form', $review_form_args);
                        ?>
                    </div>
                </div>
                <!-- Job Detail Content -->
                <!-- Job Detail SideBar -->
                <aside class="careerfy-column-4 careerfy-typo-wrap">
                    <?php do_action('jobsearch_employer_detail_side_before_contact_form', array('id' => $employer_id)); ?>

                    <div class="widget widget_your_info">
                        <?php
                        jobsearch_google_map_with_directions($employer_id);
                        ?>
                        <div class="widget_your_info_wrap">
                            <ul class="widget_your_info_list">
                                <?php
                                if (!empty($employer_address) && $all_location_allow == 'on') {
                                    ?>
                                    <li><i class="careerfy-color fa fa-map-marker"></i> <?php echo ($employer_address) ?></li>
                                    <?php
                                }
                                if (isset($user_obj->user_url) && $user_obj->user_url != '') {
                                    ?>
                                    <li><i class="careerfy-color careerfy-icon careerfy-internet"></i> <a href="<?php echo ($user_obj->user_url) ?>"><?php echo ($user_obj->user_url) ?></a></li>
                                    <?php
                                }
                                if (isset($user_obj->user_email) && $user_obj->user_email != '') {
                                    $tr_email = sprintf(__('<a href="mailto: %s">Email: %s</a>', 'wp-jobsearch'), $user_obj->user_email, $user_obj->user_email);
                                    ?>
                                    <li><i class="careerfy-color careerfy-icon careerfy-envelope"></i> <?php echo wp_kses($tr_email, array('a' => array('href' => array(), 'target' => array(), 'title' => array()))) ?></li>
                                    <?php
                                }
                                if ($employer_phone != '') {
                                    ?>
                                    <li><i class="careerfy-color careerfy-icon careerfy-technology"></i> <?php printf(esc_html__('Hotline: %s', 'wp-jobsearch'), $employer_phone) ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                            <?php
                            if ($user_facebook_url != '' || $user_twitter_url != '' || $user_linkedin_url != '' || $user_google_plus_url != '' || $user_dribbble_url != '') {
                                ?>
                                <ul class="widget_your_info_social">
                                    <?php
                                    if ($user_facebook_url != '') {
                                        ?>
                                        <li><a href="<?php echo ($user_facebook_url) ?>" data-original-title="facebook" class="fa fa-facebook"></a></li>
                                        <?php
                                    }
                                    if ($user_twitter_url != '') {
                                        ?>
                                        <li><a href="<?php echo ($user_twitter_url) ?>" data-original-title="twitter" class="fa fa-twitter"></a></li>
                                        <?php
                                    }
                                    if ($user_linkedin_url != '') {
                                        ?>
                                        <li><a href="<?php echo ($user_linkedin_url) ?>" data-original-title="linkedin" class="fa fa-linkedin"></a></li>
                                        <?php
                                    }
                                    if ($user_google_plus_url != '') {
                                        ?>
                                        <li><a href="<?php echo ($user_google_plus_url) ?>" data-original-title="google-plus" class="fa fa-google-plus"></a></li>
                                        <?php
                                    }
                                    if ($user_dribbble_url != '') {
                                        ?>
                                        <li><a href="<?php echo ($user_dribbble_url) ?>" data-original-title="dribbble" class="fa fa-dribbble"></a></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                    <?php
                    $emp_det_contact_form = isset($jobsearch_plugin_options['emp_det_contact_form']) ? $jobsearch_plugin_options['emp_det_contact_form'] : '';
                    if ($emp_det_contact_form == 'on') {
                        ob_start();
                        ?>
                        <div class="widget widget_contact_form">
                            <?php
                            $cnt_counter = rand(1000000, 9999999);
                            ?>
                            <div class="careerfy-widget-title"><h2><?php esc_html_e('Contact Form', 'wp-jobsearch') ?></h2></div>
                            <form id="ct-form-<?php echo absint($cnt_counter) ?>" data-uid="<?php echo absint($user_id) ?>" method="post">
                                <ul>
                                    <li>
                                        <input name="u_name" placeholder="<?php esc_html_e('Enter Your Name', 'wp-jobsearch') ?>" type="text">
                                        <i class="jobsearch-icon jobsearch-user"></i>
                                    </li>
                                    <li>
                                        <input name="u_email" placeholder="<?php esc_html_e('Enter Your Email Address', 'wp-jobsearch') ?>" type="text">
                                        <i class="jobsearch-icon jobsearch-mail"></i>
                                    </li>
                                    <li>
                                        <input name="u_number" placeholder="<?php esc_html_e('Enter Your Phone Number', 'wp-jobsearch') ?>" type="text">
                                        <i class="jobsearch-icon jobsearch-technology"></i>
                                    </li>
                                    <li>
                                        <textarea name="u_msg" placeholder="<?php esc_html_e('Type Your Message here', 'wp-jobsearch') ?>"></textarea>
                                    </li>
                                    <?php
                                    if ($captcha_switch == 'on') {
                                        wp_enqueue_script('jobsearch_google_recaptcha');
                                        ?>
                                        <li>
                                            <script>
                                                var recaptcha_empl_contact;
                                                var jobsearch_multicap = function () {
                                                    //Render the recaptcha_empl_contact on the element with ID "recaptcha1"
                                                    recaptcha_empl_contact = grecaptcha.render('recaptcha_empl_contact', {
                                                        'sitekey': '<?php echo ($jobsearch_sitekey); ?>', //Replace this with your Site key
                                                        'theme': 'light'
                                                    });
                                                };
                                                jQuery(document).ready(function () {
                                                    jQuery('.recaptcha-reload-a').click();
                                                });
                                            </script>
                                            <div class="recaptcha-reload" id="recaptcha_empl_contact_div">
                                                <?php echo jobsearch_recaptcha('recaptcha_empl_contact'); ?>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                    <li>
                                        <?php
                                        jobsearch_terms_and_con_link_txt();
                                        ?>
                                        <input type="submit" class="jobsearch-employer-ct-form" data-id="<?php echo absint($cnt_counter) ?>" value="<?php esc_html_e('Send now', 'wp-jobsearch') ?>">
                                        <?php
                                        $cnt__emp_wout_log = isset($jobsearch_plugin_options['emp_cntct_wout_login']) ? $jobsearch_plugin_options['emp_cntct_wout_login'] : '';
                                        if (!is_user_logged_in() && $cnt__emp_wout_log != 'on') {
                                            ?>
                                            <a class="jobsearch-open-signin-tab" style="display: none;"><?php esc_html_e('login', 'wp-jobsearch') ?></a>
                                            <?php
                                        }
                                        ?>
                                    </li>
                                </ul>
                                <span class="jobsearch-ct-msg"></span>
                            </form>
                        </div>
                        <?php
                        $emp_cntct_form = ob_get_clean();
                        echo apply_filters('jobsearch_employer_detail_cntct_frm_html', $emp_cntct_form, $employer_id);
                    }
                    ?>
                </aside>

                <?php
                //
                $default_date_time_formate = 'd-m-Y H:i:s';
                $args = array(
                    'posts_per_page' => 20,
                    'paged' => 1,
                    'post_type' => 'job',
                    'post_status' => 'publish',
                    'meta_key' => $meta_key,
                    'order' => 'DESC',
                    'orderby' => 'ID',
                    'meta_query' => array(
                        array(
                            'key' => 'jobsearch_field_job_expiry_date',
                            'value' => strtotime(current_time($default_date_time_formate, 1)),
                            'compare' => '>=',
                        ),
                        array(
                            'key' => 'jobsearch_field_job_status',
                            'value' => 'approved',
                            'compare' => '=',
                        ),
                        array(
                            'key' => 'jobsearch_field_job_posted_by',
                            'value' => $employer_id,
                            'compare' => '=',
                        ),
                    ),
                );
                $args = apply_filters('jobsearch_employer_rel_jobs_query_args', $args);
                $jobs_query = new WP_Query($args);

                if ($jobs_query->have_posts()) {
                    ?>
                    <div class="careerfy-column-12">
                        <div class="careerfy-section-title"><h2><?php printf(esc_html__('Active Jobs From %s', 'wp-jobsearch'), $user_displayname) ?></h2></div>

                        <?php
                        ob_start();
                        ?>
                        <div class="careerfy-job-listing careerfy-joblisting-view4">
                            <ul class="row">
                                <?php
                                while ($jobs_query->have_posts()) : $jobs_query->the_post();
                                    $job_id = get_the_ID();
                                    $post_thumbnail_id = jobsearch_job_get_profile_image($job_id);
                                    $post_thumbnail_image = wp_get_attachment_image_src($post_thumbnail_id, 'thumbnail');
                                    $post_thumbnail_src = isset($post_thumbnail_image[0]) && esc_url($post_thumbnail_image[0]) != '' ? $post_thumbnail_image[0] : '';

                                    $company_name = jobsearch_job_get_company_name($job_id, '@ ');
                                    $jobsearch_job_featured = get_post_meta($job_id, 'jobsearch_field_job_featured', true);
                                    $get_job_location = get_post_meta($job_id, 'jobsearch_field_location_address', true);

                                    $job_city_title = '';
                                    $get_job_city = get_post_meta($job_id, 'jobsearch_field_location_location3', true);
                                    if ($get_job_city == '') {
                                        $get_job_city = get_post_meta($job_id, 'jobsearch_field_location_location2', true);
                                    }
                                    if ($get_job_city == '') {
                                        $get_job_city = get_post_meta($job_id, 'jobsearch_field_location_location1', true);
                                    }

                                    $job_city_tax = $get_job_city != '' ? get_term_by('slug', $get_job_city, 'job-location') : '';
                                    if (is_object($job_city_tax)) {
                                        $job_city_title = $job_city_tax->name;
                                    }

                                    $sector_str = jobsearch_job_get_all_sectors($job_id, '', '', '', '<li><i class="jobsearch-icon jobsearch-filter-tool-black-shape"></i>', '</li>');

                                    $job_type_str = jobsearch_job_get_all_jobtypes($job_id, '', '', '', '', '', 'span', 'border_fill');
                                    ?>
                                    <li class="col-md-12">
                                        <div class="careerfy-joblisting-wrap">
                                            <div class="careerfy-joblisting-media">
                                                <figure><a href="<?php echo get_permalink($job_id) ?>"><img src="<?php echo ($post_thumbnail_src) ?>" alt=""></a></figure>
                                            </div>
                                            <div class="careerfy-joblisting-text">
                                                <h2><a href="<?php echo get_permalink($job_id) ?>"><?php echo get_the_title($job_id) ?></a> <?php echo ($job_type_str); ?> </h2>
                                                <?php
                                                if ($company_name != '') {
                                                    ?>
                                                    <div class="careerfy-company-name"><a><?php echo ($company_name); ?></a></div>
                                                    <?php
                                                }
                                                if (!empty($job_city_title) && $all_location_allow == 'on') {
                                                    ?>
                                                    <small><i class="careerfy-icon careerfy-maps-and-flags"></i> <?php echo esc_html($job_city_title); ?></small>
                                                    <?php
                                                }
                                                $job_skills = wp_get_post_terms($job_id, 'skill');
                                                if (!empty($job_skills)) {
                                                    ?>
                                                    <div class="careerfy-job-skills">
                                                        <?php
                                                        foreach ($job_skills as $skill_term) {
                                                            ?>
                                                            <a><?php echo ($skill_term->name) ?></a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="clearfix"></div>
                                            </div>
                                            <?php
                                            if ($jobsearch_job_featured == 'on') {
                                                ?>
                                                <span class="careerfy-joblisting-view4-featured"><?php echo esc_html__('Featured', 'wp-jobsearch'); ?></span>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </li>
                                    <?php
                                endwhile;
                                wp_reset_postdata();
                                ?>
                            </ul>
                        </div>
                        <?php
                        $activ_jobs_html = ob_get_clean();
                        echo apply_filters('jobsearch_employer_detail_active_jobs_html', $activ_jobs_html, $jobs_query);
                        ?>
                    </div>
                    <?php
                }
                ?>

            </div>
        </div>
    </div>
    <!-- Main Section -->

</div>