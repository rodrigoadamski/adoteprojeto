<?php

/**
 * Services Shortcode
 * @return html
 */
add_shortcode('careerfy_services', 'careerfy_services_shortcode');

function careerfy_services_shortcode($atts, $content = '') {
    global $service_shortcode_counter, $view, $service_text_color, $service_icon_color, $service_title_color;
    extract(shortcode_atts(array(
        'view' => '',
        'service_title_color' => '',
        'service_text_color' => '',
        'service_icon_color' => '',
                    ), $atts));

    $service_shortcode_counter = 1;

    $service_class = 'careerfy-classic-services';
    if ($view == 'view-2') {
        $service_class = 'careerfy-services-classic';
    }
    if ($view == 'view-3') {
        $service_class = 'careerfy-plain-services';
    }
    if ($view == 'view-4') {
        $service_class = 'careerfy-services careerfy-services-stylefour';
    }
    if ($view == 'view-5') {
        $service_class = 'careerfy-services careerfy-services-stylefive';
    }
    $html = '
    <div class="' . $service_class . '">
        <ul class="row">
        ' . do_shortcode($content) . '
        </ul>
    </div>' . "\n";

    return $html;
}

add_shortcode('careerfy_services_item', 'careerfy_services_item_shortcode');

function careerfy_services_item_shortcode($atts) {
    global $service_shortcode_counter, $view, $service_text_color, $service_icon_color, $service_title_color;
    extract(shortcode_atts(array(
        'service_icon' => '',
        'service_img' => '',
        'service_title' => '',
        'service_link' => '',
        'service_bg' => '',
        'service_desc' => '',
                    ), $atts));

    $icon_color = '';
    if (isset($service_icon_color) && !empty($service_icon_color)) {
        $icon_color = ' style="color:' . $service_icon_color . ' !important"';
    }

    $text_color = '';
    $title_color = '';
    $text_color_a = '';
    $text_color_h2 = '';
    if (isset($service_text_color) && !empty($service_text_color)) {
        $text_color = ' style="color:' . $service_text_color . ' !important"';
    }
    if (isset($service_title_color) && !empty($service_title_color)) {
        $title_color = ' style="color:' . $service_title_color . ' !important"';
    }


    if (isset($service_link) && !empty($service_link)) {
        $text_color_a = $title_color;
    }
    if (isset($text_color_a) && !empty($text_color_a)) {
        $text_color_h2 = '';
    } else {
        $text_color_h2 = $title_color;
    }
    if ($view == 'view-2') {
        $html = '
        <li class="col-md-4">
            <span><i class="' . $service_icon . '"' . $icon_color . '></i></span>
            <h2' . $text_color_h2 . '>' . ($service_link != '' ? '<a href="' . $service_link . '"' . $text_color_a . '>' : '') . $service_title . ($service_link != '' ? '</a>' : '') . '</h2>
            <p' . $text_color . '>' . $service_desc . '</p>
        </li>';
    } else if ($view == 'view-3' || $view == 'view-4') {
        $html = '
        <li class="col-md-4">
            ' . ($view == 'view-4' ? '<div class="careerfy-services-stylefour-wrap ' . ($service_shortcode_counter == 2 ? 'active' : '') . '"' . ($service_bg != '' ? ' style="background-color: ' . $service_bg . ';"' : '') . '>' : '') . ' 
            <i class="' . $service_icon . '"' . $icon_color . '></i>
            <h2' . $text_color_h2 . '>' . ($service_link != '' ? '<a href="' . $service_link . '"' . $text_color_a . '>' : '') . $service_title . ($service_link != '' ? '</a>' : '') . '</h2>
            <p' . $text_color . '>' . $service_desc . '</p>
            ' . ($view == 'view-4' && $service_shortcode_counter == 2 ? '<a ' . ($service_link != '' ? 'href="' . $service_link . '"' : '') . ' class="careerfy-services-stylefour-btn"><small class="careerfy-icon careerfy-right-arrow"></small></a>' : '') . '
            ' . ($view == 'view-4' ? '</div>' : '') . ' 
        </li>';
    } else if ($view == 'view-5') {
        $html = '
        <li class="col-md-4">
			<a href="' . $service_link . '"><img src="' . $service_img . '" alt=""></a>
            <h2' . $text_color_h2 . '>' . ($service_link != '' ? '<a href="' . $service_link . '"' . $text_color_a . '>' : '') . $service_title . ($service_link != '' ? '</a>' : '') . '</h2>
            <p' . $text_color . '>' . $service_desc . '</p>
        </li>';
    }	else {
        $html = '
        <li class="col-md-4">
            <span>' . $service_shortcode_counter . '</span>
            <i class="' . $service_icon . '"' . $icon_color . '></i>
            <h2' . $text_color_h2 . '>' . ($service_link != '' ? '<a href="' . $service_link . '"' . $text_color_a . '>' : '') . $service_title . ($service_link != '' ? '</a>' : '') . '</h2>
            <p' . $text_color . '>' . $service_desc . '</p>
        </li>';
    }

    $service_shortcode_counter++;

    return $html;
}
