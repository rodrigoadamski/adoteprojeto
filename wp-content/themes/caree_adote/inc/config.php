<?php

/**
 * Careerfy Theme Config.
 *
 * @package Careerfy
 */
define("CAREERFY_VERSION", "1.1.3");

function careerfy_framework_options() {
    global $careerfy_framework_options;
    return $careerfy_framework_options;
}
