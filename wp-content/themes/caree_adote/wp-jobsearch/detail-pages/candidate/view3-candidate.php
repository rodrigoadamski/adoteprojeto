<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 */
global $post, $jobsearch_plugin_options;
$candidate_id = $post->ID;
wp_enqueue_style('careerfy-candidate-detail-three');
//wp_enqueue_script('careerfy-progressbar');
wp_enqueue_script('careerfy-progressbar-two');



$careerfy__options = careerfy_framework_options();
$careerfy_theme_color = isset($careerfy__options['careerfy-main-color']) && $careerfy__options['careerfy-main-color'] != '' ? $careerfy__options['careerfy-main-color'] : '#13b5ea';



$candidate_user_id = jobsearch_get_candidate_user_id($candidate_id);
$candidates_reviews = isset($jobsearch_plugin_options['candidate_reviews_switch']) ? $jobsearch_plugin_options['candidate_reviews_switch'] : '';
$all_location_allow = isset($jobsearch_plugin_options['all_location_allow']) ? $jobsearch_plugin_options['all_location_allow'] : '';
$all_location_allow = isset($jobsearch_plugin_options['all_location_allow']) ? $jobsearch_plugin_options['all_location_allow'] : '';


$view_candidate = true;
$restrict_candidates = isset($jobsearch_plugin_options['restrict_candidates']) ? $jobsearch_plugin_options['restrict_candidates'] : '';
$restrict_candidates_for_users = isset($jobsearch_plugin_options['restrict_candidates_for_users']) ? $jobsearch_plugin_options['restrict_candidates_for_users'] : '';
$is_employer = false;
if ($restrict_candidates == 'on') {
    $view_candidate = false;
    if (is_user_logged_in()) {
        $cur_user_id = get_current_user_id();
        $cur_user_obj = wp_get_current_user();
        $employer_id = jobsearch_get_user_employer_id($cur_user_id);
        $ucandidate_id = jobsearch_get_user_candidate_id($cur_user_id);
        if ($employer_id > 0) {
            $is_employer = true;
            if ($restrict_candidates_for_users == 'register_resume') {
                $user_cv_pkg = jobsearch_employer_first_subscribed_cv_pkg();
                if ($user_cv_pkg) {
                    $view_candidate = true;
                }
            } else if ($restrict_candidates_for_users == 'only_applicants') {
                $employer_job_args = array(
                    'post_type' => 'job',
                    'posts_per_page' => '-1',
                    'post_status' => 'publish',
                    'fields' => 'ids',
                    'meta_query' => array(
                        array(
                            'key' => 'jobsearch_field_job_posted_by',
                            'value' => $employer_id,
                            'compare' => '=',
                        ),
                    ),
                );
                $employer_jobs_query = new WP_Query($employer_job_args);
                $employer_jobs_posts = $employer_jobs_query->posts;
                if (!empty($employer_jobs_posts) && is_array($employer_jobs_posts)) {
                    foreach ($employer_jobs_posts as $employer_job_id) {
                        $finded_result_list = jobsearch_find_index_user_meta_list($employer_job_id, 'jobsearch-user-jobs-applied-list', 'post_id', $candidate_user_id);
                        if (is_array($finded_result_list) && !empty($finded_result_list)) {
                            $view_candidate = true;
                            break;
                        }
                    }
                }
            } else {
                $view_candidate = true;
            }
        } else if (in_array('administrator', (array) $cur_user_obj->roles)) {
            $view_candidate = true;
        } else if ($ucandidate_id > 0 && $ucandidate_id == $candidate_id) {
            $view_candidate = true;
        }
    }
}

$captcha_switch = isset($jobsearch_plugin_options['captcha_switch']) ? $jobsearch_plugin_options['captcha_switch'] : '';
$jobsearch_sitekey = isset($jobsearch_plugin_options['captcha_sitekey']) ? $jobsearch_plugin_options['captcha_sitekey'] : '';

$plugin_default_view = isset($jobsearch_plugin_options['jobsearch-default-page-view']) ? $jobsearch_plugin_options['jobsearch-default-page-view'] : 'full';
$plugin_default_view_with_str = '';
if ($plugin_default_view == 'boxed') {

    $plugin_default_view_with_str = isset($jobsearch_plugin_options['jobsearch-boxed-view-width']) && $jobsearch_plugin_options['jobsearch-boxed-view-width'] != '' ? $jobsearch_plugin_options['jobsearch-boxed-view-width'] : '1140px';
    if ($plugin_default_view_with_str != '') {
        $plugin_default_view_with_str = ' style="width:' . $plugin_default_view_with_str . '"';
    }
}

//wp_enqueue_script('jobsearch-progressbar');

$candidate_obj = get_post($candidate_id);
$candidate_content = $candidate_obj->post_content;
$candidate_content = apply_filters('the_content', $candidate_content);
$candidate_join_date = isset($candidate_obj->post_date) ? $candidate_obj->post_date : '';
$candidate_jobtitle = get_post_meta($candidate_id, 'jobsearch_field_candidate_jobtitle', true);
$candidate_address = get_post_meta($candidate_id, 'jobsearch_field_location_address', true);
if ($candidate_address == '') {
    $candidate_address = jobsearch_job_item_address($candidate_id);
}
$user_facebook_url = get_post_meta($candidate_id, 'jobsearch_field_user_facebook_url', true);
$user_twitter_url = get_post_meta($candidate_id, 'jobsearch_field_user_twitter_url', true);
$user_google_plus_url = get_post_meta($candidate_id, 'jobsearch_field_user_google_plus_url', true);
$user_youtube_url = get_post_meta($candidate_id, 'jobsearch_field_user_youtube_url', true);
$user_dribbble_url = get_post_meta($candidate_id, 'jobsearch_field_user_dribbble_url', true);
$user_linkedin_url = get_post_meta($candidate_id, 'jobsearch_field_user_linkedin_url', true);
$user_id = jobsearch_get_candidate_user_id($candidate_id);
$user_obj = get_user_by('ID', $user_id);
$user_displayname = isset($user_obj->display_name) ? $user_obj->display_name : '';
$user_displayname = apply_filters('jobsearch_user_display_name', $user_displayname, $user_obj);
$user_def_avatar_url = get_avatar_url($user_id, array('size' => 350, 450));

$user_avatar_id = get_post_thumbnail_id($candidate_id);
if ($user_avatar_id > 0) {
    $user_thumbnail_image = wp_get_attachment_image_src($user_avatar_id, 'full');
    $user_def_avatar_url = isset($user_thumbnail_image[0]) && esc_url($user_thumbnail_image[0]) != '' ? $user_thumbnail_image[0] : '';
}
$user_def_avatar_url = $user_def_avatar_url == '' ? jobsearch_candidate_image_placeholder() : $user_def_avatar_url;
wp_enqueue_script('isotope-min');


$sectors = wp_get_post_terms($candidate_id, 'sector');
ob_start();
$html = '';
$page_id = isset($jobsearch_plugin_options['jobsearch_cand_result_page']) ? $jobsearch_plugin_options['jobsearch_cand_result_page'] : '';
$page_id = jobsearch__get_post_id($page_id, 'page');
$page_id = jobsearch_wpml_lang_page_id($page_id, 'page');
$result_page = get_permalink($page_id);
if (!empty($sectors)) {
    $link_class_str = 'careerfy-right';
    foreach ($sectors as $term) :
        ?>
        <a href="<?php echo add_query_arg(array('sector_cat' => $term->slug), $result_page); ?>" class="<?php echo ($link_class_str) ?>">
            <?php
            echo sprintf(esc_html__('see more %s candidates', 'careerfy'), $term->name);
            ?>
            <i class="jobsearch-icon jobsearch-arrows22"></i>
        </a>
        <?php
    endforeach;
}
$html .= ob_get_clean();
$sector_str = jobsearch_job_get_all_sectors($candidate_id, '', '  ', '', '<li>', '</li>');
?>
<!-- SubHeader -->
<div class="careerfy-subheader-style7">
    <!-- SubHeader Style7 Top -->
    <div class="careerfy-subheader-style7-top">
        <div class="container">
            <div class="row">
                <div class="careerfy-column-12">
                    <a href="javascript:history.back(1);" class="careerfy-left"><i class="jobsearch-icon jobsearch-arrows22"></i><?php echo esc_html__(' Back to all Candidates', 'careerfy'); ?></a>
                    <?php echo ($html); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- SubHeader Style7 Top -->
    <div class="careerfy-breadcrumb-style7">
        <div class="container">
            <ul>
                <li><a href="<?php echo home_url($candidate_id); ?>">Home</a></li>
                <li><a href="<?php echo esc_url($result_page); ?>"><?php echo get_the_title($page_id); ?></a></li>
                <?php echo ($sector_str); ?>
                <li><?php echo get_the_title($candidate_id); ?></li>
            </ul>
        </div>
    </div>

</div>
<!-- SubHeader -->

<!-- Main Content -->
<div class="careerfy-main-content">

    <!-- Main Section -->
    <div class="careerfy-main-section">
        <div class="container">
            <div class="row">

                <!-- Job Detail Content -->
                <div class="careerfy-column-8">
                    <div class="careerfy-typo-wrap">
                        <div class="careerfy-candidatedetail-content3">
                            <figure class="careerfy-candidatedetail-text3">
                                <?php
                                $show_disp_name = apply_filters('jobsearch_candidate_detail_content_top_displayname', $user_displayname, $candidate_id);
                                ?>
                                <img src="<?php echo ($user_def_avatar_url) ?>" alt="">
                                <figcaption>
                                    <h2><?php echo ($show_disp_name); ?></h2>
                                    <p><?php echo ($candidate_jobtitle) ?></p>
                                    <ul class="careerfy-candidatedetail3-list">
                                        <?php
                                        if ($candidate_address != '' && $all_location_allow == 'on') {
                                            ?>
                                            <li><strong><?php echo esc_html__('Location:', 'careerfy'); ?></strong></li>
                                            <li><?php echo ($candidate_address) ?></li>
                                            <?php
                                        }
                                        if ($candidate_join_date != '') {
                                            //$candidate_join_date = jobsearch_time_elapsed_string($candidate_join_date, '');
                                            ?>
                                            <li><strong><?php echo esc_html__('Adotante desde :', 'careerfy'); ?></strong></li>
                                            <li><?php echo esc_html(human_time_diff(strtotime($candidate_join_date), current_time('timestamp'))) . ' atrás'; ?></li>
                                            <?php
                                        }
                                        ?>
                                    </ul>
                                    <?php
                                    do_action('jobsearch_download_candidate_cv_btn', array('id' => $candidate_id, 'classes' => 'careerfy-candidatedetail-text3-btn'));

                                    if ($user_facebook_url != '' || $user_twitter_url != '' || $user_linkedin_url != '' || $user_google_plus_url != '' || $user_dribbble_url != '') {
                                        ?>
                                        <ul class="careerfy-candidatedetail3-social">
                                            <?php
                                            if ($user_facebook_url != '') {
                                                ?>
                                                <li><a href="<?php echo ($user_facebook_url) ?>" data-original-title="facebook" class="fa fa-facebook"></a></li>
                                                <?php
                                            }
                                            if ($user_twitter_url != '') {
                                                ?>
                                                <li><a href="<?php echo ($user_twitter_url) ?>" data-original-title="twitter" class="fa fa-twitter"></a></li>
                                                <?php
                                            }
                                            if ($user_linkedin_url != '') {
                                                ?>
                                                <li><a href="<?php echo ($user_linkedin_url) ?>" data-original-title="linkedin" class="fa fa-linkedin"></a></li>
                                                <?php
                                            }
                                            if ($user_google_plus_url != '') {
                                                ?>
                                                <li><a href="<?php echo ($user_google_plus_url) ?>" data-original-title="google-plus" class="fa fa-google-plus"></a></li>
                                                <?php
                                            }
                                            if ($user_dribbble_url != '') {
                                                ?>
                                                <li><a href="<?php echo ($user_dribbble_url) ?>" data-original-title="dribbble" class="fa fa-dribbble"></a></li>
                                                <?php
                                            }
                                            ?>
                                        </ul>
                                        <?php
                                    }
                                    ?>
                                </figcaption>
                            </figure>
                        </div>

                        <?php
                        if ($candidate_content != '') {
                            ?>
                            <div class = "careerfy-jobdetail-content careerfy-candidatedetail-content3">
                                <div class = "careerfy-content-title"><h2><?php esc_html_e('Descrição', 'wp-jobsearch') ?></h2></div>
                                <div class="careerfy-description">
                                    <?php echo ($candidate_content) ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>


                        <?php
                        $exfield_list = get_post_meta($candidate_id, 'jobsearch_field_education_title', true);
                        $exfield_list_val = get_post_meta($candidate_id, 'jobsearch_field_education_description', true);
                        $education_academyfield_list = get_post_meta($candidate_id, 'jobsearch_field_education_academy', true);
                        $education_yearfield_list = get_post_meta($candidate_id, 'jobsearch_field_education_year', true);

                        ob_start();
                        if (!empty($exfield_list)) {
                            ?>
                            <div class = "careerfy-candidatedetail-content3">
                                <div class="careerfy-candidate-title"> <h2><i class="jobsearch-icon jobsearch-mortarboard"></i> <?php esc_html_e('Education', 'wp-jobsearch') ?></h2> </div>
                                <div class="careerfy-candidate-timeline-two">
                                    <ul class="careerfy-row">
                                        <?php
                                        $exfield_counter = 0;
                                        foreach ($exfield_list as $exfield) {
                                            $exfield_val = isset($exfield_list_val[$exfield_counter]) ? $exfield_list_val[$exfield_counter] : '';
                                            $education_academyfield_val = isset($education_academyfield_list[$exfield_counter]) ? $education_academyfield_list[$exfield_counter] : '';
                                            $education_yearfield_val = isset($education_yearfield_list[$exfield_counter]) ? $education_yearfield_list[$exfield_counter] : '';
                                            ?>
                                            <li class="careerfy-column-12">
                                                <div class="careerfy-candidate-timeline-two-text">
                                                    <span><?php echo ($education_academyfield_val) ?><small><?php echo ($education_yearfield_val) ?></small></span>
                                                    <h2><a><?php echo ($exfield) ?></a></h2>
                                                    <p><?php echo ($exfield_val) ?></p>
                                                </div>
                                            </li>
                                            <?php
                                            $exfield_counter ++;
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                            <?php
                        }
                        $edu_html = ob_get_clean();
                        echo apply_filters('jobsearch_candidate_detail_education_html', $edu_html, $candidate_id);
                        ?>


                        <?php
                        // experience

                        $exfield_list = get_post_meta($candidate_id, 'jobsearch_field_experience_title', true);
                        $exfield_list_val = get_post_meta($candidate_id, 'jobsearch_field_experience_description', true);
                        $experience_start_datefield_list = get_post_meta($candidate_id, 'jobsearch_field_experience_start_date', true);
                        $experience_end_datefield_list = get_post_meta($candidate_id, 'jobsearch_field_experience_end_date', true);
                        $experience_company_field_list = get_post_meta($candidate_id, 'jobsearch_field_experience_company', true);
                        if (is_array($exfield_list) && sizeof($exfield_list) > 0) {

                            $exfield_counter = 0;
                            ?>
                            <div class = "careerfy-candidatedetail-content3">
                                <div class="careerfy-candidate-title"> <h2><i class="jobsearch-icon jobsearch-social-media"></i> <?php esc_html_e('Experience', 'wp-jobsearch') ?></h2> </div>
                                <div class="careerfy-candidate-timeline-two">
                                    <ul class="careerfy-row">
                                        <?php
                                        foreach ($exfield_list as $exfield) {
                                            $exfield_val = isset($exfield_list_val[$exfield_counter]) ? $exfield_list_val[$exfield_counter] : '';
                                            $experience_start_datefield_val = isset($experience_start_datefield_list[$exfield_counter]) ? $experience_start_datefield_list[$exfield_counter] : '';
                                            $experience_end_datefield_val = isset($experience_end_datefield_list[$exfield_counter]) ? $experience_end_datefield_list[$exfield_counter] : '';
                                            $experience_end_companyfield_val = isset($experience_company_field_list[$exfield_counter]) ? $experience_company_field_list[$exfield_counter] : '';
                                            ?>
                                            <li class="careerfy-column-12">
                                                <div class="careerfy-candidate-timeline-two-text">
                                                    <span><?php echo ($experience_end_companyfield_val) ?><small><?php echo ($experience_start_datefield_val != '' ? date('Y', strtotime($experience_start_datefield_val)) : '') . ($experience_end_datefield_val != '' ? ' - ' . date('Y', strtotime($experience_end_datefield_val)) : '') ?></small></span> 
                                                    <h2><a><?php echo ($exfield) ?></a></h2>
                                                    <p><?php echo ($exfield_val) ?></p>
                                                </div>


                                            </li>
                                            <?php
                                            $exfield_counter ++;
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                            <?php
                        }
                        ?>


                        <?php
                        $exfield_list = get_post_meta($candidate_id, 'jobsearch_field_skill_title', true);
                        $skill_percentagefield_list = get_post_meta($candidate_id, 'jobsearch_field_skill_percentage', true);
                        if (is_array($exfield_list) && sizeof($exfield_list) > 0) {
                            ?>
                            <div class = "careerfy-candidatedetail-content3">
                                <div class="careerfy-candidate-title"> <h2><i class="jobsearch-icon jobsearch-design-skills"></i> <?php esc_html_e('Habilidades', 'wp-jobsearch') ?></h2> </div>
                                <?php
                                $exfield_counter = 0;
                                foreach ($exfield_list as $exfield) {
                                    $rand_num = rand(1000000, 99999999);
                                    $skill_percentagefield_val = isset($skill_percentagefield_list[$exfield_counter]) ? absint($skill_percentagefield_list[$exfield_counter]) : '';
                                    $skill_percentagefield_val = $skill_percentagefield_val > 100 ? 100 : $skill_percentagefield_val;
                                    ?>
                                    <div class = "careerfy_progressbar_two_wrap">
                                        <div class = "careerfy_progressbar_two" data-width = '<?php echo ($skill_percentagefield_val) ?>'><?php echo ($exfield) ?></div>
                                    </div>
                                    <script>
                                        jQuery(document).ready(function ($) {
                                            jQuery('.careerfy_progressbar_two').progressBar({
                                                percentage: true,
                                                backgroundColor: "#e4e8e9",
                                                barColor: "<?php echo ($careerfy_theme_color); ?>",
                                                animation: true,
                                                height: "40",
                                            });
                                        });
                                    </script>
                                    <?php
                                    $exfield_counter ++;
                                }
                                ?>
                            </div>
                            <?php
                        }
                        ?>


                        <?php
                        // portfolio

                        $exfield_list = get_post_meta($candidate_id, 'jobsearch_field_portfolio_title', true);
                        $exfield_list_val = get_post_meta($candidate_id, 'jobsearch_field_portfolio_image', true);
                        $exfield_portfolio_url = get_post_meta($candidate_id, 'jobsearch_field_portfolio_url', true);
                        $exfield_portfolio_vurl = get_post_meta($candidate_id, 'jobsearch_field_portfolio_vurl', true);
                        if (is_array($exfield_list) && sizeof($exfield_list) > 0) {
                            ?>
                            <div class = "careerfy-candidatedetail-content3 careerfy-simple-gallery-main">
                                <div class="careerfy-candidate-title"> <h2><i class="jobsearch-icon jobsearch-briefcase"></i> <?php esc_html_e('Portfolio', 'wp-jobsearch') ?></h2> </div>
                                <div class="careerfy-gallery careerfy-simple-gallery candidate_portfolio">
                                    <ul class="careerfy-row">
                                        <?php
                                        $exfield_counter = 0;
                                        foreach ($exfield_list as $exfield) {
                                            $rand_num = rand(1000000, 99999999);
                                            $portfolio_img = isset($exfield_list_val[$exfield_counter]) ? $exfield_list_val[$exfield_counter] : '';
                                            $portfolio_url = isset($exfield_portfolio_url[$exfield_counter]) ? $exfield_portfolio_url[$exfield_counter] : '';
                                            $portfolio_vurl = isset($exfield_portfolio_vurl[$exfield_counter]) ? $exfield_portfolio_vurl[$exfield_counter] : '';
                                            if ($portfolio_vurl != '') {
                                                if (strpos($portfolio_vurl, 'watch?v=') !== false) {
                                                    $portfolio_vurl = str_replace('watch?v=', 'embed/', $portfolio_vurl);
                                                }
                                                if (strpos($portfolio_vurl, '?') !== false) {
                                                    $portfolio_vurl .= '&autoplay=1';
                                                } else {
                                                    $portfolio_vurl .= '?autoplay=1';
                                                }
                                            }
                                            $port_thumb_img = $portfolio_img;
                                            if ($portfolio_img != '') {
                                                $attach_id = jobsearch_get_attachment_id_from_url($portfolio_img);
                                                $port_thumb_image = wp_get_attachment_image_src($attach_id, 'large');
                                                $port_thumb_img = isset($port_thumb_image[0]) && esc_url($port_thumb_image[0]) != '' ? $port_thumb_image[0] : $portfolio_img;
                                            }

                                            $col_class = 'careerfy-column-4';
                                            if ($exfield_counter == 0) {
                                                $col_class = 'careerfy-column-6';
                                            } elseif ($exfield_counter > 0 && $exfield_counter < 5) {
                                                $col_class = 'careerfy-column-3';
                                            }
                                            ?>
                                            <li class="<?php echo ($col_class) ?>">
                                                <a href="<?php echo ($portfolio_vurl != '' ? $portfolio_vurl : $portfolio_img) ?>" class="<?php echo ($portfolio_vurl != '' ? 'fancybox-video' : 'fancybox') ?>" title="<?php echo ($exfield) ?>" <?php echo ($portfolio_vurl != '' ? 'data-fancybox-type="iframe"' : '') ?> data-fancybox-group="group">
                                                    <img src="<?php echo ($portfolio_img) ?>" alt="">
                                                </a> 
                                            </li>
                                            <?php
                                            $exfield_counter ++;
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                            <?php
                        }
                        ?>


                        <?php
                        $exfield_list = get_post_meta($candidate_id, 'jobsearch_field_award_title', true);
                        $exfield_list_val = get_post_meta($candidate_id, 'jobsearch_field_award_description', true);
                        $award_yearfield_list = get_post_meta($candidate_id, 'jobsearch_field_award_year', true);
                        if (is_array($exfield_list) && sizeof($exfield_list) > 0) {
                            ?>
                            <div class = "careerfy-candidatedetail-content3">
                                <div class="careerfy-candidate-title"> <h2><i class="jobsearch-icon jobsearch-trophy"></i> <?php esc_html_e('Honors & awards', 'wp-jobsearch') ?></h2> </div>
                                <div class="careerfy-candidate-timeline-two">
                                    <ul class="careerfy-row">
                                        <?php
                                        $exfield_counter = 0;
                                        foreach ($exfield_list as $exfield) {
                                            $rand_num = rand(1000000, 99999999);
                                            $exfield_val = isset($exfield_list_val[$exfield_counter]) ? $exfield_list_val[$exfield_counter] : '';
                                            $award_yearfield_val = isset($award_yearfield_list[$exfield_counter]) ? $award_yearfield_list[$exfield_counter] : '';
                                            ?>
                                            <li class="careerfy-column-12">
                                                <div class="careerfy-candidate-timeline-two-text">
                                                    <span><?php echo ($exfield) ?> <small><?php echo ($award_yearfield_val) ?></small></span> 
                                                    <h2><a href="javascript:void(0)"><?php echo ($exfield) ?></a></h2>
                                                    <p><?php echo ($exfield_val) ?></p>
                                                </div>
                                            </li>
                                            <?php
                                            $exfield_counter ++;
                                        }
                                        ?>
                                    </ul>
                                </div>
                            </div>
                            <?php
                        }
                        if ($candidates_reviews == 'on') {
                            ?>
                            <div class = "careerfy-candidatedetail-content3">
                                <?php
                                $post_reviews_args = array(
                                    'post_id' => $candidate_id,
                                    'list_label' => esc_html__('Candidate Reviews', 'wp-jobsearch'),
                                );
                                do_action('jobsearch_post_reviews_list', $post_reviews_args);

                                $review_form_args = array(
                                    'post_id' => $candidate_id,
                                    'must_login' => 'no',
                                );
                                do_action('jobsearch_add_review_form', $review_form_args);
                                ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <!--Job Detail Content -->
                <!--Job Detail SideBar -->
                <aside class = "careerfy-column-4">
                    <div class = "careerfy-typo-wrap">
                        <?php
                        $custom_all_fields = get_option('jobsearch_custom_field_candidate');
                        if (!empty($custom_all_fields)) {
                            ?>
                            <div class = "careerfy-widget-title"><h2><?php echo esc_html__('Candidate Overview', 'careerfy'); ?></h2></div>
                            <div class="widget careerfy-candidatedetail-services">
                                <ul>
                                    <?php
                                    $cus_fields = array('content' => '');
                                    $cus_fields = apply_filters('jobsearch_custom_fields_list', 'candidate', $candidate_id, $cus_fields, '<li>', '</li>', '', true, true, true, 'careerfy');
                                    if (isset($cus_fields['content']) && $cus_fields['content'] != '') {
                                        echo ($cus_fields['content']);
                                    }
                                    ?>
                                </ul>
                            </div>
                            <?php
                        }
                        $cand_det_contact_form = isset($jobsearch_plugin_options['cand_det_contact_form']) ? $jobsearch_plugin_options['cand_det_contact_form'] : '';
                        if ($cand_det_contact_form == 'on') {
                            ob_start();
                            ?>
                            <div class="widget widget_contact_form">
                                <?php
                                $cnt_counter = rand(1000000, 9999999);
                                ?>
                                <div class="careerfy-widget-title"><h2><?php esc_html_e('Contact Form', 'wp-jobsearch') ?></h2></div>
                                <form id="ct-form-<?php echo absint($cnt_counter) ?>" data-uid="<?php echo absint($user_id) ?>" method="post">
                                    <ul>
                                        <li>
                                            <label><?php esc_html_e('User Name:', 'wp-jobsearch') ?></label>
                                            <input name="u_name" placeholder="<?php esc_html_e('Enter Your Name', 'wp-jobsearch') ?>" type="text">
                                            <i class="jobsearch-icon jobsearch-user"></i>
                                        </li>
                                        <li>
                                            <label><?php esc_html_e('Email Address:', 'wp-jobsearch') ?></label>
                                            <input name="u_email" placeholder="<?php esc_html_e('Enter Your Email Address', 'wp-jobsearch') ?>" type="text">
                                            <i class="jobsearch-icon jobsearch-mail"></i>
                                        </li>
                                        <li>
                                            <label><?php esc_html_e('Phone Number:', 'wp-jobsearch') ?></label>
                                            <input name="u_number" placeholder="<?php esc_html_e('Enter Your Phone Number', 'wp-jobsearch') ?>" type="text">
                                            <i class="jobsearch-icon jobsearch-technology"></i>
                                        </li>
                                        <li>
                                            <label><?php esc_html_e('Message:', 'wp-jobsearch') ?></label>
                                            <textarea name="u_msg" placeholder="<?php esc_html_e('Type Your Message here', 'wp-jobsearch') ?>"></textarea>
                                        </li>
                                        <?php
                                        if ($captcha_switch == 'on') {
                                            wp_enqueue_script('jobsearch_google_recaptcha');
                                            ?>
                                            <li>
                                                <script>
                                                    var recaptcha_cand_contact;
                                                    var jobsearch_multicap = function () {
                                                        //Render the recaptcha_cand_contact on the element with ID "recaptcha1"
                                                        recaptcha_cand_contact = grecaptcha.render('recaptcha_cand_contact', {
                                                            'sitekey': '<?php echo ($jobsearch_sitekey); ?>', //Replace this with your Site key
                                                            'theme': 'light'
                                                        });
                                                    };
                                                    jQuery(document).ready(function () {
                                                        jQuery('.recaptcha-reload-a').click();
                                                    });
                                                </script>
                                                <div class="recaptcha-reload" id="recaptcha_cand_contact_div">
                                                    <?php echo jobsearch_recaptcha('recaptcha_cand_contact'); ?>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                        <li>
                                            
                                            <input type="submit" class="jobsearch-candidate-ct-form" data-id="<?php echo absint($cnt_counter) ?>" value="<?php esc_html_e('Enviar', 'wp-jobsearch') ?>">
                                            <?php
                                            $cnt__cand_wout_log = isset($jobsearch_plugin_options['cand_cntct_wout_login']) ? $jobsearch_plugin_options['cand_cntct_wout_login'] : '';
                                            if (!is_user_logged_in() && $cnt__cand_wout_log != 'on') {
                                                ?>
                                                <a class="jobsearch-open-signin-tab" style="display: none;"><?php esc_html_e('login', 'wp-jobsearch') ?></a>
                                                <?php
                                            }
                                            ?>
                                        </li>
                                    </ul>
                                    <span class="jobsearch-ct-msg"></span>
                                </form>
                            </div>
                            <?php
                            $cand_cntct_form = ob_get_clean();
                            echo apply_filters('jobsearch_candidate_detail_cntct_frm_html', $cand_cntct_form, $candidate_id);
                        }
                        ?>
                    </div>
                </aside>
                <!--Job Detail SideBar -->

            </div>
        </div>
    </div>
    <!--Main Section -->

</div>
<!--Main Content -->



